from mongoengine import *

db_db = "justeem"
db_host = "localhost:27017"
connect(db=db_db, host=db_host)

NOTETYPE = ('HTML', 'VOICE')

class MeetingNote(Document):
    _class = StringField(required=False)
    type = StringField(required=True, choices=NOTETYPE)
    title = StringField(required=True)
    note = StringField(required=False)
    meetingId = StringField(required=True)
    owner = StringField(required=True)
    meta = {'collection': 'meetingNote'}

if __name__ == "__main__":
    objects = MeetingNote.objects()
    print(objects[0].type)