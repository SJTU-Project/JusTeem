# -*- coding: utf-8 -*-

from flask import Flask, jsonify
import speech_recognition
import text_structure
import dao
import thread
import time

app = Flask(__name__)
# 使 jsonify 能够返回中文
app.config['JSON_AS_ASCII'] = False

@app.route('/recognize/<note_id>/<filename>')
def recognize_controller(note_id, filename):
    return jsonify(recognize(note_id, filename))

def recognize(note_id, filename):
    note = dao.MeetingNote.objects(id=note_id)
    if len(note) == 0:
        return {"status": "error", "detail": "Note not exist"}
    note = note[0]

    if note.type != "VOICE":
        return {"status": "error", "detail": "Not a voice note"}

    result = speech_recognition.recognize(filename)
    if result["status"] != "error":
        thread.start_new_thread(speech_recognition.checkAndDelete, (result["taskid"], note, filename))
    return result

@app.route('/check/<file_name>/<taskid>')
def check_controller(file_name, taskid):
    return jsonify(speech_recognition.check(file_name, taskid))

@app.route('/result/<file_name>/<taskid>')
def result_controller(file_name, taskid):
    return jsonify(speech_recognition.result(file_name, taskid))

@app.route('/tag/<text>')
def tag_controller(text):
    return jsonify(text_structure.tag(text))

if __name__=="__main__":
    # note_id = "5e130640ac7c9908c05c6fa0"
    # file_name = "test.aac"
    # recognize("5e130640ac7c9908c05c6fa0", file_name)
    # time.sleep(300)
    app.run(host="0.0.0.0", port=5000, debug=True)
