package com.sjtu.project.meetingservice.domain;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class AttendantTest {

    @Test
    public void testEquals() {
        Attendant a = new Attendant();
        a.setUsername("a");
        Attendant b = new Attendant();
        b.setUsername("a");
        Assert.assertEquals(a, b);
    }

    @Test
    public void testContains() {
        Attendant a = new Attendant();
        a.setUsername("a");
        Attendant b = new Host();
        b.setUsername("a");
        List<Attendant> attendantList = Arrays.asList(a);
        Assert.assertTrue(attendantList.contains(b));
    }
}