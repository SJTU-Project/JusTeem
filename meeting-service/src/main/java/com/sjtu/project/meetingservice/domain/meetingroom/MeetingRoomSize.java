package com.sjtu.project.meetingservice.domain.meetingroom;

public enum MeetingRoomSize {
    BIG,
    MIDDLE,
    SMALL
}
