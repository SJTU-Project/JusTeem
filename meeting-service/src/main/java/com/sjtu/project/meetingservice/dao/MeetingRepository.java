package com.sjtu.project.meetingservice.dao;

import com.sjtu.project.meetingservice.domain.Meeting;
import com.sjtu.project.meetingservice.domain.Status;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeetingRepository extends MongoRepository<Meeting, String>, CustomMeetingRepository{
    Meeting findOneByAttendNumAndStatusIs(String attendNum, Status status);

    Meeting findOneById(String id);

    Meeting findOneByIdAndStatusIs(String id, Status pending);
}
