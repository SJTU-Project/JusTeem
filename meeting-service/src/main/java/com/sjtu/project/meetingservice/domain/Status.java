package com.sjtu.project.meetingservice.domain;

public enum Status {
    Pending,
    ComingSoon,
    Running,
    Cancelled,
    Stopped
}
