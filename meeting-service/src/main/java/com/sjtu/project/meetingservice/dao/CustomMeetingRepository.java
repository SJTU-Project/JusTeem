package com.sjtu.project.meetingservice.dao;

import com.sjtu.project.meetingservice.domain.Attendant;
import com.sjtu.project.meetingservice.domain.Meeting;
import com.sjtu.project.meetingservice.domain.QueryParam;

import java.util.List;

public interface CustomMeetingRepository {
    List<Meeting> findAllByDateAndAttendantContains(String date, Attendant attendant);

    List<Meeting> queryMeetings(QueryParam queryParam);

    List<Meeting> findMeetingsToAttend();
}
