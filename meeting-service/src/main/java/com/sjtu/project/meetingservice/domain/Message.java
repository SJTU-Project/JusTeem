package com.sjtu.project.meetingservice.domain;

import lombok.Data;

@Data
public class Message {
    String content;
}
