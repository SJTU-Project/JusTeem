package com.sjtu.project.meetingservice.dao;

import com.sjtu.project.common.util.UserUtil;
import com.sjtu.project.meetingservice.domain.Attendant;
import com.sjtu.project.meetingservice.domain.Meeting;
import com.sjtu.project.meetingservice.domain.QueryParam;
import com.sjtu.project.meetingservice.domain.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomMeetingRepositoryImpl implements CustomMeetingRepository{
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Meeting> findAllByDateAndAttendantContains(String date, Attendant attendant) {
        Query query = new Query();
        query.addCriteria(new Criteria().andOperator(
                Criteria.where("date").regex(date),
                Criteria.where("attendants").elemMatch(Criteria.where("username").is(attendant.getUsername()))
        ));
        if (!date.equals(".*")) {
            query.with(new Sort(Sort.Direction.ASC, "timeRange.start"));
        }
        return mongoTemplate.find(query, Meeting.class);
    }

    @Override
    public List<Meeting> queryMeetings(QueryParam queryParam) {
        Query query = new Query();
        if (!queryParam.getAttendant().getUsername().equals("admin")) {
            query.addCriteria(Criteria.where("attendants").elemMatch(Criteria.where("username").is(queryParam.getAttendant().getUsername())));
        }
        if (queryParam.getStatus() != null){
            query.addCriteria(Criteria.where("status").in(queryParam.getStatus()));
        }
        return mongoTemplate.find(query, Meeting.class);
    }

    @Override
    public List<Meeting> findMeetingsToAttend() {
        List<Status> statuses = new ArrayList<>();
        statuses.add(Status.Pending);

        Query query = new Query();
        query.addCriteria(Criteria.where("status").in(statuses));
        query.addCriteria(Criteria.where("attendants").not()
                .elemMatch(Criteria.where("username").is(UserUtil.getUsername())));
        return mongoTemplate.find(query, Meeting.class);
    }
}
