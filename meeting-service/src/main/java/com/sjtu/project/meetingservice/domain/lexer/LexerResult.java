package com.sjtu.project.meetingservice.domain.lexer;

import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.meetingservice.domain.meetingroom.Requirement;
import lombok.Data;

@Data
public class LexerResult {
    int startTime;

    int endTime;

    String date;

    String error;

    public Requirement convert2Requirement() {
        Requirement requirement = new Requirement();
        requirement.setDate(getDate());
        requirement.setTimeRange(new TimeRange(startTime, endTime));
        return requirement;
    }
}
