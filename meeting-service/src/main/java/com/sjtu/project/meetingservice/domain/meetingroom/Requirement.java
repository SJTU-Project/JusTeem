package com.sjtu.project.meetingservice.domain.meetingroom;

import com.sjtu.project.common.entity.TimeRange;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Requirement {
    String heading;

    String description;

    String date;

    TimeRange timeRange;

    MeetingRoomSize size;

    List<MeetingRoomDevice> meetingRoomDeviceList = new ArrayList<>();
}
