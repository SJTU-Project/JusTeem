package com.sjtu.project.meetingservice.controller;

import com.sjtu.project.common.response.Result;
import com.sjtu.project.common.util.ResultUtil;
import com.sjtu.project.meetingservice.domain.*;
import com.sjtu.project.meetingservice.domain.meetingroom.Requirement;
import com.sjtu.project.meetingservice.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class MeetingController {
    @Autowired
    MeetingService meetingService;

    @GetMapping("/meeting/{id}")
    public Result<Meeting> findOneMeeting(@PathVariable String id) {
        return ResultUtil.success(meetingService.findOneMeeting(id));
    }

    @GetMapping("/meeting/query")
    public Result<List<Meeting>> queryMeetings(QueryParam queryParam){
        return ResultUtil.success(meetingService.queryMeetings(queryParam));
    }

    @GetMapping("/meeting")
    public Result<List<Meeting>> findUserMeetings(@RequestParam(required = false, defaultValue = ".*") String date) {
        return ResultUtil.success(meetingService.findUserMeetings(date));
    }

    @GetMapping("/meeting/all")
    public Result<List<Meeting>> findAll() {
        return ResultUtil.success(meetingService.findAllMeetings());
    }


    @PostMapping("/meeting")
    public Result<Meeting> createMeeting(@Valid @RequestBody Meeting meeting) {
        return ResultUtil.success(meetingService.addMeeting(meeting));
    }

    @PutMapping("/meeting")
    public Result<Meeting> modifyMeeting(@RequestBody Meeting meeting) {
        return ResultUtil.success(meetingService.modifyMeeting(meeting));
    }

    @PostMapping("/meeting/fuzzy")
    public Result<Meeting> creatingMeetingFuzzily(@RequestBody Requirement requirement) {
        return ResultUtil.success(meetingService.createMeetingFuzzily(requirement));
    }

    @PostMapping("/meeting/fuzzy2")
    public Result<Meeting> creatingMeetingFuzzily2(@RequestBody Requirement requirement) {
        return ResultUtil.success(meetingService.createMeetingFuzzily2(requirement));
    }

    @PostMapping("/meeting/intelligent")
    public Result<Meeting> creatingMeetingIntelligently(@RequestBody Message message) {
        return ResultUtil.success(meetingService.createMeetingIntelligently(message));
    }

    @PostMapping("/meeting/{id}/attendants")
    public Result<Meeting> addAttendants(@PathVariable String id,
                                         @RequestBody List<Attendant> attendants) {
        return ResultUtil.success(meetingService.addAttendants(id, attendants));
    }

    @PostMapping("/meeting/{id}/attendant")
    public Result<Meeting> attend2Meeting(@PathVariable String id,
                                          @RequestParam(name = "attendType") AttendType attendType) {
        return ResultUtil.success(meetingService.attendMeeting(id, attendType));
    }

    @DeleteMapping("meeting/{id}/attendant")
    public Result<Meeting> removeAttendant(@PathVariable String id,
                                           @RequestParam(name = "username") String username){
        return ResultUtil.success(meetingService.removeAttendant(id, username));
    }

    @DeleteMapping("/meeting/{id}/exit")
    public Result<Meeting> exitMeeting(@PathVariable String id) {
        return ResultUtil.success(meetingService.exitMeeting(id));
    }

    @PutMapping("/meeting/{id}/status")
    public Result<Meeting> modifyMeetingStatus(@PathVariable String id,
                                               @RequestParam(name = "status") Status status) {
        return ResultUtil.success(meetingService.modifyMeetingStatus(id, status));
    }

    @GetMapping("/meeting/{id}/status")
    public Result<Status> getMeetingStatus(@PathVariable String id) {
        return ResultUtil.success(meetingService.findOneMeeting(id).getStatus());
    }

    @GetMapping("/meeting/attend")
    public Result<List<Meeting>> findMeetingsToAttend(){
        return ResultUtil.success(meetingService.findMeetingsToAttend());
    }
}
