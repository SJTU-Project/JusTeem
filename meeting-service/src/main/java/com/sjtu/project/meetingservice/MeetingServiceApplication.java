package com.sjtu.project.meetingservice;


import com.sjtu.project.common.annotation.JusTeemApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@JusTeemApplication
@EnableFeignClients
public class MeetingServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(MeetingServiceApplication.class, args);
    }
}
