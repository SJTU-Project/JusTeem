package com.sjtu.project.meetingservice.domain;

import lombok.Data;

import java.util.Objects;

@Data
public class Attendant {
    String username;

    String signInTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Attendant attendant = (Attendant) o;
        return username.equals(attendant.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    public Attendant withUsername(String username) {
        this.username = username;
        return this;
    }
}
