package com.sjtu.project.meetingservice.service;

import com.sjtu.project.meetingservice.domain.lexer.LexerResult;

public interface LexerService {
    LexerResult analyzeMessage(String message);
}
