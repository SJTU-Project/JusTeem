package com.sjtu.project.meetingservice.domain;

import lombok.Data;

import java.util.List;

@Data
public class QueryParam {
    List<Status> status;
    Attendant attendant;
}
