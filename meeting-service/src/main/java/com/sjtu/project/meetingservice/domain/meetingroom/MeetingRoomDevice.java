package com.sjtu.project.meetingservice.domain.meetingroom;

import lombok.Getter;

public enum MeetingRoomDevice {
    AIRCONDITIONER("空调"),
    BLACKBOARD("黑板"),
    TABLE("桌子"),
    PROJECTOR("投影仪"),
    POWERSUPPLY("电源");

    @Getter
    String name;

    MeetingRoomDevice(String name) {
        this.name = name;
    }
}
