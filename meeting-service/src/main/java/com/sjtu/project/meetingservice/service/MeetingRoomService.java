package com.sjtu.project.meetingservice.service;

import com.google.common.net.HttpHeaders;
import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.common.response.Result;
import com.sjtu.project.meetingservice.domain.Meeting;
import com.sjtu.project.meetingservice.domain.meetingroom.Requirement;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "meetingroom-service")
public interface MeetingRoomService {
    @RequestMapping(value = "/meetingroom/{id}/schedule/{date}/timeSlice", method = RequestMethod.PUT)
    @ResponseBody
    Result modifySchedule(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                          @PathVariable("id") String id,
                          @PathVariable("date") String date,
                          @RequestParam("occupied") boolean occupied,
                          @RequestBody TimeRange timeRange);


    @PostMapping("/schedule/query")
    @ResponseBody
    Result<Meeting> getMeetingRoomConditional(@RequestHeader(HttpHeaders.AUTHORIZATION) String token, @RequestBody Requirement requirement);
}
