package com.sjtu.project.meetingservice.service.impl;

import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.common.exception.NoSuitableObjectException;
import com.sjtu.project.common.exception.ObjectNotFoundException;
import com.sjtu.project.common.exception.ServiceInvokedFailureException;
import com.sjtu.project.common.response.Result;
import com.sjtu.project.common.response.ResultCode;
import com.sjtu.project.common.util.DateUtil;
import com.sjtu.project.common.util.JWTUtil;
import com.sjtu.project.common.util.TimeRangeUtil;
import com.sjtu.project.common.util.UserUtil;
import com.sjtu.project.meetingservice.dao.MeetingRepository;
import com.sjtu.project.meetingservice.domain.*;
import com.sjtu.project.meetingservice.domain.lexer.LexerResult;
import com.sjtu.project.meetingservice.domain.meetingroom.Requirement;
import com.sjtu.project.meetingservice.service.LexerService;
import com.sjtu.project.meetingservice.service.MeetingRoomService;
import com.sjtu.project.meetingservice.service.MeetingService;
import com.sjtu.project.meetingservice.service.SchedulerService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static com.sjtu.project.common.util.ResultUtil.SPLITTER;

@Service
@Slf4j
public class MeetingServiceImpl implements MeetingService {
    @Autowired
    MeetingRepository meetingRepository;

    @Autowired
    MeetingRoomService meetingRoomService;

    @Autowired
    SchedulerService schedulerService;

    @Autowired
    LexerService lexerService;

    @Override
    public List<Meeting> findAllMeetings() {
        return meetingRepository.findAll();
    }

    @Override
    public Meeting findOneMeeting(String id) {
        Meeting res = meetingRepository.findOneById(id);
        if (res == null) {
            throw new ObjectNotFoundException("meeting");
        }
        return res;
    }

    public List<Meeting> findUserMeetings(String date) {
        Attendant attendant = new Attendant().withUsername(UserUtil.getUsername());
        return meetingRepository.findAllByDateAndAttendantContains(date, attendant);
    }

    @Override
    public List<Meeting> queryMeetings(QueryParam queryParam) {
        Attendant attendant = new Attendant().withUsername(UserUtil.getUsername());
        queryParam.setAttendant(attendant);
        return meetingRepository.queryMeetings(queryParam);
    }

    @Override
    public List<Meeting> findMeetingsToAttend() {

        return meetingRepository.findMeetingsToAttend();
    }

    public Meeting addMeeting(Meeting meeting) {
        if (!meeting.getHost().getUsername().equals(UserUtil.getUsername())) {
            throw new IllegalArgumentException("Host" + SPLITTER + "Host should be the api invoker");
        }

        if (!meeting.getAttendants().contains(meeting.getHost())) {
            throw new IllegalArgumentException("Attendants" + SPLITTER + "Host should be in the attendants");
        }

        DateUtil.validateDate(meeting.getDate());

        TimeRangeUtil.validateTimeRange(meeting.getTimeRange());

        validateStartTime(meeting.getDate(), meeting.getTimeRange());

        Result result = meetingRoomService.modifySchedule(UserUtil.getToken(),
                meeting.getMeetingRoomId(),
                meeting.getDate(),
                true,
                meeting.getTimeRange());

        if (result.getCode() != ResultCode.SUCCESS.getCode()) {
            throw new ServiceInvokedFailureException("MeetingRoomService" + SPLITTER + result.getMessage());
        }

        meeting.setId(null);
        meeting.setAttendNum(generateFourRandomNumber());
        meeting.setStatus(Status.Pending);
        meetingRepository.save(meeting);

        result = schedulerService.createSchedule(UserUtil.getToken(), meeting);
        if (result.getCode() != ResultCode.SUCCESS.getCode()) {
            throw new ServiceInvokedFailureException("MeetingRoomService" + SPLITTER + result.getMessage());
        }
        return meeting;
    }

    private String generateFourRandomNumber() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<4; ++i) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    private boolean isStartTimeBeforeNow(String date, TimeRange timeRange) {
        String time = date + " " + TimeRangeUtil.convertDateNumber2String(timeRange.getStart());
        Date startDate = DateUtil.convertTime2Date(time);
        return startDate.before(new Date());
    }

    @Override
    public Meeting createMeetingFuzzily(Requirement requirement) {
        log.info("invoke meeting room service with {}", requirement);
        if (requirement == null) {
            throw new IllegalArgumentException("requirement" + SPLITTER + "requirement should not be null");
        }

        if (!StringUtils.isEmpty(requirement.getDate())) {
            DateUtil.validateDate(requirement.getDate());
        }

        if (requirement.getTimeRange() != null) {
            TimeRangeUtil.validateTimeRange(requirement.getTimeRange());
        }

        if (requirement.getTimeRange() != null && !StringUtils.isEmpty(requirement.getDate())) {
            validateStartTime(requirement.getDate(), requirement.getTimeRange());
        }

        Result<Meeting> result = meetingRoomService.getMeetingRoomConditional(UserUtil.getToken(), requirement);
        log.info("result of invoking meeting room service {}", result);

        if (result.getCode() != ResultCode.SUCCESS.getCode()) {
            throw new ServiceInvokedFailureException("MeetingRoomService" + SPLITTER + result.getMessage());
        }
        if (result.getData() == null) {
            throw new NoSuitableObjectException();
        }
        return result.getData();
    }

    @Override
    public Meeting createMeetingFuzzily2(Requirement requirement) {
        Meeting meeting = createMeetingFuzzily(requirement);
        meeting.setHeading(requirement.getHeading());
        meeting.setDescription(requirement.getDescription());

        Host attendant = (Host) new Host().withUsername(UserUtil.getUsername());
        meeting.setHost(attendant);
        meeting.setAttendants(new ArrayList<>());
        meeting.addAttendant(attendant);
        return addMeeting(meeting);

    }

    private void validateStartTime(String date, TimeRange timeRange) {
        if (isStartTimeBeforeNow(date, timeRange)) {
            throw new IllegalArgumentException("TimeRange" + SPLITTER + "Start time should not before now");
        }
    }

    @Override
    public Meeting createMeetingIntelligently(Message message) {
        LexerResult res = lexerService.analyzeMessage(message.getContent());
        if (!StringUtils.isEmpty(res.getError())) {
            throw new ServiceInvokedFailureException("LexerService" + SPLITTER + res.getError());
        }
        return createMeetingFuzzily(res.convert2Requirement());
    }

    @Override
    public Meeting addAttendants(String meetingId, List<Attendant> attendants) {
        Meeting meeting = meetingRepository.findOneById(meetingId);
        verifyInvokerIsHost(meeting);
        verifyCurrentStatusIsTrue(meeting.getStatus() == Status.Pending);
        meeting.addAttendants(attendants);
        return meetingRepository.save(meeting);
    }

    @Override
    public Meeting exitMeeting(String meetingId) {
        Meeting meeting = meetingRepository.findOneById(meetingId);
        meeting.removeAttendants(new Attendant().withUsername(UserUtil.getUsername()));
        return meetingRepository.save(meeting);
    }

    @Override
    public Meeting removeAttendant(String meetingId, String username) {
        Meeting meeting = meetingRepository.findOneById(meetingId);
        verifyInvokerIsHost(meeting);
        verifyCurrentStatusIsTrue(meeting.getStatus() == Status.Pending);
        meeting.removeAttendants(new Attendant().withUsername(username));
        return meetingRepository.save(meeting);
    }

    @Override
    public Meeting modifyMeeting(Meeting meeting) {
        Meeting originalOne = meetingRepository.findOneById(meeting.getId());
        meetingRoomService.modifySchedule(JWTUtil.adminToken, originalOne.getMeetingRoomId(), originalOne.getDate(), false, originalOne.getTimeRange());
        meetingRoomService.modifySchedule(JWTUtil.adminToken, meeting.getMeetingRoomId(), meeting.getDate(), true, meeting.getTimeRange());

        originalOne.setDate(meeting.getDate());
        originalOne.setTimeRange(meeting.getTimeRange());
        originalOne.setDescription(meeting.getDescription());
        originalOne.setHeading(meeting.getHeading());

        return meetingRepository.save(originalOne);
    }

    private boolean invokerIsHost(Meeting meeting) {
        return meeting.getHost().getUsername().equals(UserUtil.getUsername());
    }

    @Override
    public Meeting attendMeeting(String id, AttendType attendType) {
        Meeting meeting = null;
        switch (attendType) {
            case MEETING_ID:
                meeting = meetingRepository.findOneByIdAndStatusIs(id, Status.Pending);
                break;
            case ATTEND_NUM:
                meeting = meetingRepository.findOneByAttendNumAndStatusIs(id, Status.Pending);
                break;
        }
        if (meeting == null) {
            throw new ObjectNotFoundException("meeting");
        }
        verifyCurrentStatusIsTrue(meeting.getStatus() == Status.Pending);
        meeting.addAttendant(new Attendant().withUsername(UserUtil.getUsername()));
        return meetingRepository.save(meeting);
    }

    @Override
    //TODO 使用状态模式进行重构
    public Meeting modifyMeetingStatus(String id, Status status) {
        Meeting meeting = findOneMeeting(id);
        if (!UserUtil.getUsername().equals("admin")) {
            verifyStatusChange(meeting, status);
        }
        switch (status) {
            case Stopped:
                stopMeeting(meeting);
                break;
            case Cancelled:
                cancelMeeting(meeting);
                break;
            case ComingSoon:
            case Running:
                break;
            default:
                throw new IllegalArgumentException("Status" + SPLITTER + "The status is not suit for the state graph");
        }
        meeting.setStatus(status);
        return meetingRepository.save(meeting);
    }

    private void verifyStatusChange(Meeting meeting, Status status) {
        switch (status) {
            case ComingSoon:
                verifyCurrentStatusIsTrue(meeting.getStatus() == Status.Pending);
                break;
            case Running:
                verifyCurrentStatusIsTrue(meeting.getStatus() == Status.ComingSoon);
                verifyInvokerIsHost(meeting);
                break;
            case Stopped:
                verifyCurrentStatusIsTrue(meeting.getStatus() == Status.Running);
                verifyInvokerIsHost(meeting);
                break;
            case Cancelled:
                verifyCurrentStatusIsTrue(meeting.getStatus() == Status.Pending || meeting.getStatus() == Status.ComingSoon);
                verifyInvokerIsHost(meeting);
                break;
            default:
                throw new IllegalArgumentException("Status" + SPLITTER + "The status is not suit for the state graph");
        }

    }

    private void stopMeeting(Meeting meeting) {
        int dateNumber = TimeRangeUtil.convertCurrentTime2DateNumber();
        if (dateNumber < meeting.getTimeRange().getEnd()) {
            Result result = meetingRoomService.modifySchedule(UserUtil.getToken(),
                    meeting.getMeetingRoomId(),
                    meeting.getDate(),
                    false,
                    new TimeRange(dateNumber, meeting.getTimeRange().getEnd()));
            if (result.getCode() != ResultCode.SUCCESS.getCode()) {
                throw new ServiceInvokedFailureException("MeetingRoomService" + SPLITTER + result.getMessage());
            }
        }

        schedulerService.cancelSchedule(UserUtil.getToken(), meeting.getId());
        meeting.setStatus(Status.Stopped);
        meetingRepository.save(meeting);
    }

    private void verifyInvokerIsHost(Meeting meeting) {
        if (!invokerIsHost(meeting)) {
            throw new AccessDeniedException("Invoker should be the host");
        }
    }

    private void verifyCurrentStatusIsTrue(boolean condition) {
        if (!condition) {
            throw new IllegalArgumentException("Status" + SPLITTER + "The status is not suit for the state graph");
        }
    }

    private void cancelMeeting(Meeting meeting) {
        Result result = meetingRoomService.modifySchedule(UserUtil.getToken(),
                meeting.getMeetingRoomId(),
                meeting.getDate(),
                false,
                meeting.getTimeRange());

        if (result.getCode() != ResultCode.SUCCESS.getCode()) {
            throw new ServiceInvokedFailureException("MeetingRoomService" + SPLITTER + result.getMessage());
        }
        schedulerService.cancelSchedule(UserUtil.getToken(), meeting.getId());
        meeting.setStatus(Status.Cancelled);
        meetingRepository.save(meeting);
    }

}
