package com.sjtu.project.meetingservice.service;

import com.sjtu.project.common.response.Result;
import com.sjtu.project.meetingservice.domain.Meeting;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "scheduler-service")
public interface SchedulerService {
    @PostMapping("/schedule")
    Result<Meeting> createSchedule(@RequestHeader(HttpHeaders.AUTHORIZATION) String token, @RequestBody Meeting meeting);

    @DeleteMapping("/schedule/{meetingId}")
    Result<Meeting> cancelSchedule(@RequestHeader(HttpHeaders.AUTHORIZATION) String token, @PathVariable(name = "meetingId") String meetingId);
}
