package com.sjtu.project.meetingservice.domain;

import com.sjtu.project.common.entity.TimeRange;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Document
@Data
public class Meeting {
    @Id
    String id;

    @NotBlank
    String heading;

    @NotBlank
    String description;

    @NotBlank
    String date;

    @NotNull
    TimeRange timeRange;

    @NotBlank
    String meetingRoomId;

    @NotBlank
    String meetingRoomLocation;

    String attendNum;

    Status status;

    @NotNull
    Host host;

    @NotNull
    List<Attendant> attendants;

    public void addAttendant(Attendant attendant) {
        if (!attendants.contains(attendant)) {
            attendants.add(attendant);
        }
    }

    public void removeAttendants(Attendant attendant){
        attendants.remove(attendant);
    }

    public void addAttendants(List<Attendant> attendants) {
        attendants.forEach(this::addAttendant);
    }
}
