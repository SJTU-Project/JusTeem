package com.sjtu.project.meetingservice.domain;

import lombok.Data;

import java.util.Objects;

@Data
public class Host extends Attendant{
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}
