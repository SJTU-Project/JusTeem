package com.sjtu.project.meetingservice.service.impl;

import com.sjtu.project.meetingservice.domain.lexer.LexerResult;
import com.sjtu.project.meetingservice.service.LexerService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class LexerServiceImpl implements LexerService {
    @Override
    public LexerResult analyzeMessage(String message) {
        RestTemplate restTemplate = new RestTemplate();
        String sb = "http://47.106.8.44:4000/lexer" +
                "/" +
                message;
        return restTemplate.exchange(sb,
                HttpMethod.GET,
                new HttpEntity<>(new HttpHeaders()),
                LexerResult.class).getBody();
    }
}