package com.sjtu.project.meetingservice.service;

import com.sjtu.project.meetingservice.domain.*;
import com.sjtu.project.meetingservice.domain.meetingroom.Requirement;

import java.util.List;

public interface MeetingService {
    Meeting findOneMeeting(String id);

    List<Meeting> findAllMeetings();

    List<Meeting> findUserMeetings(String date);

    List<Meeting> queryMeetings(QueryParam queryParam);

    Meeting addMeeting(Meeting meeting);

    Meeting modifyMeeting(Meeting meeting);

    Meeting createMeetingFuzzily(Requirement requirement);

    Meeting createMeetingFuzzily2(Requirement requirement);

    Meeting createMeetingIntelligently(Message message);

    Meeting addAttendants(String meetingId, List<Attendant> attendants);

    Meeting attendMeeting(String id, AttendType attendType);

    Meeting exitMeeting(String id);

    Meeting removeAttendant(String id, String username);

    Meeting modifyMeetingStatus(String id, Status status);

    List<Meeting> findMeetingsToAttend();
}
