package com.sjtu.project.schedulerservice.domain;

import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.schedulerservice.domain.commandparameter.OperationParameter;
import com.sjtu.project.schedulerservice.core.StartTimeCalculator;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ScheduledMeeting {
    String id;

    String meetingRoomId;

    TimeRange timeRange;

    String date;

    String token;

    List<OperationParameter> operations;

    public ScheduledMeeting withId(String meetingId){
        this.id = meetingId;
        return this;
    }


    public Date calculateStartTimeByJob(Class<? extends StartTimeCalculator> jobClazz){
        try {
            return jobClazz.newInstance().calculateStartTime(date, timeRange);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<OperationParameter> getOperations() {
        return operations;
    }

    public void setOperations(List<OperationParameter> operations) {
        this.operations = operations;
    }
}
