package com.sjtu.project.schedulerservice.core.job;

import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.common.util.DateUtil;
import com.sjtu.project.common.util.TimeRangeUtil;
import com.sjtu.project.schedulerservice.core.StartTimeCalculator;
import com.sjtu.project.schedulerservice.domain.ScheduledMeeting;
import com.sjtu.project.schedulerservice.service.JobService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MeetingEndJob implements Job, StartTimeCalculator {
    @Autowired
    JobService jobService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        try {
            ScheduledMeeting scheduledMeeting = (ScheduledMeeting) jobExecutionContext.getJobDetail().getJobDataMap().get("meeting");
            jobService.whenMeetingEnd(scheduledMeeting);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Date calculateStartTime(String date, TimeRange timeRange) {
        String startTime = date + " " + TimeRangeUtil.convertDateNumber2String(timeRange.getEnd());
        return DateUtil.convertTime2Date(startTime);
    }
}
