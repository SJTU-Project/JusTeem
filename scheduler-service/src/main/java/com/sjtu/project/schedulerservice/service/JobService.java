package com.sjtu.project.schedulerservice.service;

import com.sjtu.project.schedulerservice.domain.ScheduledMeeting;

public interface JobService {
    void beforeMeetingStart30min(ScheduledMeeting scheduledMeeting);

    void whenMeetingStart(ScheduledMeeting scheduledMeeting);

    void afterMeetingStart15min(ScheduledMeeting scheduledMeeting);

    void whenMeetingEnd(ScheduledMeeting scheduledMeeting);
}
