package com.sjtu.project.schedulerservice.service;

import com.google.common.net.HttpHeaders;
import com.sjtu.project.common.response.Result;
import com.sjtu.project.schedulerservice.domain.MeetingStatus;
import com.sjtu.project.schedulerservice.domain.ScheduledMeeting;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "meeting-service")
public interface MeetingService {
    @RequestMapping(value = "/meeting/{id}/status", method = RequestMethod.PUT)
    @ResponseBody
    Result modifyMeetingStatus(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                               @PathVariable(name = "id") String id,
                               @RequestParam(name = "status") MeetingStatus status);

    @GetMapping("/meeting/{id}/status")
    @ResponseBody
    Result<MeetingStatus> getMeetingStatus(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                           @PathVariable(name = "id") String id);

    @GetMapping("/meeting/query")
    Result<List<ScheduledMeeting>> queryMeetings(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                                 @RequestParam(name = "status") MeetingStatus status);
}

