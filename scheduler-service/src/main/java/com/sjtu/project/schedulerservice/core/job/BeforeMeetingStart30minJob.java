package com.sjtu.project.schedulerservice.core.job;

import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.common.util.DateUtil;
import com.sjtu.project.common.util.TimeRangeUtil;
import com.sjtu.project.schedulerservice.core.StartTimeCalculator;
import com.sjtu.project.schedulerservice.domain.ScheduledMeeting;
import com.sjtu.project.schedulerservice.service.JobService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class BeforeMeetingStart30minJob implements Job, StartTimeCalculator {
    @Autowired
    JobService jobService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        try {
            /**
             * Retrieve meeting parameters from JobDataMap
             */
            ScheduledMeeting scheduledMeeting = (ScheduledMeeting) jobExecutionContext.getJobDetail().getJobDataMap().get("meeting");
            jobService.beforeMeetingStart30min(scheduledMeeting);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Date calculateStartTime(String date, TimeRange timeRange) {
        String startTime = date + " " + TimeRangeUtil.convertDateNumber2String(timeRange.getStart() - 1);
        return DateUtil.convertTime2Date(startTime);
    }
}
