package com.sjtu.project.schedulerservice.core.job;

import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.common.util.DateUtil;
import com.sjtu.project.common.util.TimeRangeUtil;
import com.sjtu.project.schedulerservice.core.StartTimeCalculator;
import com.sjtu.project.schedulerservice.domain.ScheduledMeeting;
import com.sjtu.project.schedulerservice.service.JobService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Calendar;
import java.util.Date;

@Component
public class AfterMeetingStart15minJob implements Job, StartTimeCalculator {
    @Autowired
    JobService jobService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        Map<String, ScheduledMeeting> meetingMap = (Map) jobExecutionContext.getJobDetail().getJobDataMap().get("meetingMap");
        ScheduledMeeting scheduledMeeting = (ScheduledMeeting) jobExecutionContext.getJobDetail().getJobDataMap().get("meeting");
        try {
            jobService.afterMeetingStart15min(scheduledMeeting);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            meetingMap.remove(scheduledMeeting.getId());
        }
    }

    @Override
    public Date calculateStartTime(String date, TimeRange timeRange) {
        String startTime = date + " " + TimeRangeUtil.convertDateNumber2String(timeRange.getStart());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtil.convertTime2Date(startTime));
        calendar.add(Calendar.MINUTE, 15);
        return calendar.getTime();
    }
}
