package com.sjtu.project.schedulerservice.core;

import com.sjtu.project.common.entity.TimeRange;

import java.util.Date;

public interface StartTimeCalculator {
    Date calculateStartTime(String date, TimeRange timeRange);
}
