package com.sjtu.project.schedulerservice.service.impl;

import com.sjtu.project.common.exception.ServiceInvokedFailureException;
import com.sjtu.project.common.response.Result;
import com.sjtu.project.common.response.ResultCode;
import com.sjtu.project.common.util.DateUtil;
import com.sjtu.project.common.util.JWTUtil;
import com.sjtu.project.common.util.TimeRangeUtil;
import com.sjtu.project.schedulerservice.core.SchedulerManager;
import com.sjtu.project.schedulerservice.domain.ScheduledMeeting;
import com.sjtu.project.schedulerservice.service.MeetingService;
import com.sjtu.project.schedulerservice.service.SchedulerService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

import static com.sjtu.project.common.util.ResultUtil.SPLITTER;
import static com.sjtu.project.schedulerservice.domain.MeetingStatus.Cancelled;
import static com.sjtu.project.schedulerservice.domain.MeetingStatus.Pending;

@Service
@Slf4j
public class ScheduledServiceImpl implements SchedulerService {
    @Autowired
    SchedulerManager schedulerManager;

    @Autowired
    MeetingService meetingService;

    @PostConstruct
    public void initializeUnScheduledMeeting() {
        new Thread(() -> {
            Result<List<ScheduledMeeting>> res = meetingService.queryMeetings(JWTUtil.adminToken, Pending);
            if (res.getCode() != ResultCode.SUCCESS.getCode()) {
                throw new ServiceInvokedFailureException("SchedulerService" + SPLITTER + res.getMessage());
            }
            else {
                for (ScheduledMeeting scheduledMeeting : res.getData()) {
                    Date now = new Date();
                    if (DateUtil.convertTime2Date(scheduledMeeting.getDate() + " "
                            + TimeRangeUtil.convertDateNumber2String(scheduledMeeting.getTimeRange().getStart())).before(now)) {
                        meetingService.modifyMeetingStatus(JWTUtil.adminToken, scheduledMeeting.getId(), Cancelled);
                    }
                    else {
                        log.info("Schedule a meeting {}", scheduledMeeting.getId());
                        createScheduledMeeting(scheduledMeeting);
                    }
                }
            }
        }).start();
    }

    @Override
    public ScheduledMeeting createScheduledMeeting(ScheduledMeeting scheduledMeeting) {
        try {
            schedulerManager.submit(scheduledMeeting);
            return scheduledMeeting;
        }
        catch (SchedulerException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ScheduledMeeting cancelScheduledMeeting(String meetingId) {
        try{
            return schedulerManager.cancel(meetingId);
        }
        catch (SchedulerException e){
            return null;
        }
    }

    @Override
    public ScheduledMeeting modifyScheduledMeeting(String meetingId, ScheduledMeeting scheduledMeeting) {
        try{
            return schedulerManager.modify(meetingId, scheduledMeeting);
        }
        catch (SchedulerException e){
            return null;
        }
    }
}
