package com.sjtu.project.schedulerservice.domain;

public enum MeetingStatus {
    Pending,
    ComingSoon,
    Running,
    Cancelled,
    Stopped
}
