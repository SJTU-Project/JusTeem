package com.sjtu.project.schedulerservice.service;

import com.google.common.net.HttpHeaders;
import com.sjtu.project.common.response.Result;
import com.sjtu.project.schedulerservice.domain.commandparameter.OperationParameter;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "iot-service")
public interface IotService {
    @RequestMapping(value = "/operation/{meetingRoomId}", method = RequestMethod.POST)
    @ResponseBody
    Result operateDevice(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                         @PathVariable(name = "meetingRoomId") String meetingRoomId,
                         @RequestBody List<OperationParameter> params);
}

