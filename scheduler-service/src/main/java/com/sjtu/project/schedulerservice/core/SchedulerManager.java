package com.sjtu.project.schedulerservice.core;

import com.sjtu.project.common.util.JWTUtil;
import com.sjtu.project.common.util.UserUtil;
import com.sjtu.project.schedulerservice.core.job.BeforeMeetingStart30minJob;
import com.sjtu.project.schedulerservice.core.job.MeetingEndJob;
import com.sjtu.project.schedulerservice.core.job.AfterMeetingStart15minJob;
import com.sjtu.project.schedulerservice.domain.ScheduledMeeting;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

import static org.quartz.JobBuilder.*;

@Component
public class SchedulerManager {

    @Autowired
    private Scheduler scheduler;

    private final Class[] jobList = {
            BeforeMeetingStart30minJob.class,
            AfterMeetingStart15minJob.class,
            MeetingEndJob.class
    };

    /**
     * Key: id
     * Value: scheduledMeeting (i.e. scheduled task)
     */
    private ConcurrentHashMap<String, ScheduledMeeting> meetingMap = new ConcurrentHashMap<>();

    public void submit(ScheduledMeeting meeting) throws SchedulerException{
        if (meetingMap.containsKey(meeting.getId()))
            return;


        /**
         * Token cannot be fetched from Job context
         * so this is a temporary solution to get the token
         */
        meeting.setToken(JWTUtil.adminToken);

        scheduleMeeting(meeting);
        meetingMap.put(meeting.getId(), meeting);
    }

    public ScheduledMeeting modify(String meetingId, ScheduledMeeting meeting) throws SchedulerException{
        ScheduledMeeting oldMeeting = meetingMap.get(meetingId);
        if (oldMeeting == null)
            return null;


        if (oldMeeting.getMeetingRoomId().equals(meeting.getMeetingRoomId())){
            /**
             * RoomId is not changed
             */
            rescheduleMeeting(meeting);
        }
        else{
            /**
             * RoomId is changed
             */
            meeting.setToken(UserUtil.getToken());
            cancel(meetingId);
            submit(meeting);
        }

        return meeting;
    }

    public ScheduledMeeting cancel(String meetingId) throws SchedulerException{
        ScheduledMeeting meeting = meetingMap.get(meetingId);
        if (meeting == null)
            return null;

        cancelMeeting(meeting);
        meetingMap.remove(meetingId);
        return meeting;
    }

    private void rescheduleMeeting(ScheduledMeeting meeting) throws SchedulerException{
        for (Class job: jobList){
            rescheduleMeetingJob(meeting, job);
        }
    }

    private void rescheduleMeetingJob(ScheduledMeeting meeting, Class jobClazz) throws SchedulerException{
        TriggerKey triggerKey = TriggerKey.triggerKey(
                getTriggerName(meeting, jobClazz),
                getTriggerGroup(meeting, jobClazz)
        );
        Trigger oldTrigger = scheduler.getTrigger(triggerKey);
        if (oldTrigger == null){
            return;
        }

        Trigger newTrigger = TriggerBuilder.newTrigger()
                .withIdentity(getTriggerName(meeting, jobClazz), getTriggerGroup(meeting, jobClazz))
                .withDescription(String.format("<%s> trigger for Meeting <%s> ", jobClazz.getName(), meeting.getId()))
                .startAt(meeting.calculateStartTimeByJob(jobClazz))
                .build();

        scheduler.rescheduleJob(triggerKey, newTrigger);

    }

    private void cancelMeeting(ScheduledMeeting meeting) throws SchedulerException{
        for (Class job: jobList){
            cancelMeetingJob(meeting, job);
        }
    }

    private void cancelMeetingJob(ScheduledMeeting meeting, Class jobClazz) throws SchedulerException{
        TriggerKey triggerKey = TriggerKey.triggerKey(
                getTriggerName(meeting, jobClazz),
                getTriggerGroup(meeting, jobClazz)
        );
        scheduler.pauseTrigger(triggerKey);
        scheduler.unscheduleJob(triggerKey);

        JobKey jobKey = JobKey.jobKey(
                getJobName(meeting, jobClazz),
                getJobGroup(meeting, jobClazz));
        scheduler.deleteJob(jobKey);
    }

    private void scheduleMeeting(ScheduledMeeting meeting) throws SchedulerException{
        for (Class job: jobList){
            scheduleMeetingJob(meeting, job);
        }
    }

    private void scheduleMeetingJob(ScheduledMeeting meeting, Class jobClazz) throws SchedulerException {
        /**
         * Pass parameters to Job by JobDataMap
         */
        JobDataMap map = new JobDataMap();
        map.put("meeting", meeting);
        map.put("meetingMap", meetingMap);

        JobDetail detail = newJob(jobClazz)
                .withIdentity(getJobName(meeting, jobClazz), getJobGroup(meeting, jobClazz))
                .withDescription(String.format("<%s> for Meeting <%s> ", jobClazz.getName(), meeting.getId()))
                .usingJobData(map)
                .build();
        Trigger trigger = TriggerBuilder.newTrigger().forJob(detail)
                .withIdentity(getTriggerName(meeting, jobClazz), getTriggerGroup(meeting, jobClazz))
                .withDescription(String.format("<%s> trigger for Meeting <%s> ", jobClazz.getName(), meeting.getId()))
                .startAt(meeting.calculateStartTimeByJob(jobClazz))
                .build();
        scheduler.scheduleJob(detail, trigger);
    }

    private String getJobName(ScheduledMeeting meeting, Class jobClazz){
        return meeting.getId()+"-"+jobClazz.getName();
    }

    private String getJobGroup(ScheduledMeeting meeting, Class jobClazz){
        return jobClazz.getName();
    }

    private String getTriggerName(ScheduledMeeting meeting, Class jobClazz){
        return getJobName(meeting, jobClazz) + "-trigger";
    }

    private String getTriggerGroup(ScheduledMeeting meeting, Class jobClazz){
        return getJobGroup(meeting, jobClazz) + "-trigger";
    }
}
