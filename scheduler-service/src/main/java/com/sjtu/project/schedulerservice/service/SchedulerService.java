package com.sjtu.project.schedulerservice.service;

import com.sjtu.project.schedulerservice.domain.ScheduledMeeting;

public interface SchedulerService {
    ScheduledMeeting createScheduledMeeting(ScheduledMeeting scheduledMeeting);

    ScheduledMeeting cancelScheduledMeeting(String meetingId);

    ScheduledMeeting modifyScheduledMeeting(String meetingId, ScheduledMeeting scheduledMeeting);
}
