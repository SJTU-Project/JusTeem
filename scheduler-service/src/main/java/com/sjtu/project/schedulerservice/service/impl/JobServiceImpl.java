package com.sjtu.project.schedulerservice.service.impl;

import com.sjtu.project.schedulerservice.domain.ScheduledMeeting;
import com.sjtu.project.schedulerservice.service.IotService;
import com.sjtu.project.schedulerservice.domain.MeetingStatus;
import com.sjtu.project.schedulerservice.service.JobService;
import com.sjtu.project.schedulerservice.service.MeetingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class JobServiceImpl implements JobService {
    @Autowired
    MeetingService meetingService;

    @Autowired
    IotService iotService;

    @Override
    public void beforeMeetingStart30min(ScheduledMeeting scheduledMeeting) {
        log.info("30min before meeting started {}", scheduledMeeting.getId());
        meetingService.modifyMeetingStatus(scheduledMeeting.getToken(),
                scheduledMeeting.getId(),
                MeetingStatus.ComingSoon);
    }

    @Override
    public void whenMeetingStart(ScheduledMeeting scheduledMeeting) {
        System.out.println("Meeting started " + scheduledMeeting.getId());
        if (scheduledMeeting.getOperations() == null || scheduledMeeting.getOperations().size() == 0) {
            return;
        }

        iotService.operateDevice(scheduledMeeting.getToken(),
                scheduledMeeting.getMeetingRoomId(),
                scheduledMeeting.getOperations());
    }

    @Override
    public void afterMeetingStart15min(ScheduledMeeting scheduledMeeting) {
        /**
         * If meeting is running then return;
         * else stop the meeting
         */
        log.info("15min After meeting started  {}", scheduledMeeting.getId());
        MeetingStatus status = meetingService.getMeetingStatus(scheduledMeeting.getToken(), scheduledMeeting.getId()).getData();
        if (status != MeetingStatus.Running) {
            meetingService.modifyMeetingStatus(scheduledMeeting.getToken(),
                    scheduledMeeting.getId(),
                    MeetingStatus.Cancelled);
        }
    }

    @Override
    public void whenMeetingEnd(ScheduledMeeting scheduledMeeting) {
        log.info("Meeting finished {}", scheduledMeeting.getId());
        MeetingStatus status = meetingService.getMeetingStatus(scheduledMeeting.getToken(), scheduledMeeting.getId()).getData();
        if (status != MeetingStatus.Stopped) {
            meetingService.modifyMeetingStatus(scheduledMeeting.getToken(),
                    scheduledMeeting.getId(),
                    MeetingStatus.Stopped);
        }
    }
}
