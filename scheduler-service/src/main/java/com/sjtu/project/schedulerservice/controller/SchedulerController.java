package com.sjtu.project.schedulerservice.controller;

import com.sjtu.project.common.response.Result;
import com.sjtu.project.common.response.ResultCode;
import com.sjtu.project.common.util.ResultUtil;
import com.sjtu.project.schedulerservice.domain.ScheduledMeeting;
import com.sjtu.project.schedulerservice.service.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class SchedulerController {
    @Autowired
    SchedulerService schedulerService;


    @PostMapping("/schedule")
    public Result<ScheduledMeeting> createSchedule(@RequestBody ScheduledMeeting scheduledMeeting){
        ScheduledMeeting res = schedulerService.createScheduledMeeting(scheduledMeeting);
        return res == null ? ResultUtil.failure(ResultCode.FAILURE):
                ResultUtil.success(res);
    }

    @DeleteMapping("/schedule/{meetingId}")
    public Result<ScheduledMeeting> cancelSchedule(@PathVariable String meetingId){
        ScheduledMeeting res = schedulerService.cancelScheduledMeeting(meetingId);
        return res == null ? ResultUtil.failure(ResultCode.FAILURE):
                ResultUtil.success(res);
    }
}
