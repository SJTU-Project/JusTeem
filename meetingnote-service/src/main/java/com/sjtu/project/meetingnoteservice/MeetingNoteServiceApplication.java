package com.sjtu.project.meetingnoteservice;

import com.sjtu.project.common.annotation.JusTeemApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@JusTeemApplication
@EnableFeignClients
public class MeetingNoteServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(MeetingNoteServiceApplication.class, args);
    }
}
