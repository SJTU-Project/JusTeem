package com.sjtu.project.meetingnoteservice.transactionImpl;

import com.sjtu.project.common.util.ResultUtil;
import com.sjtu.project.meetingnoteservice.abstractTransactions.AddMeetingNoteTransaction;
import com.sjtu.project.meetingnoteservice.common.Constants;
import com.sjtu.project.meetingnoteservice.dao.MeetingNoteRepository;
import com.sjtu.project.meetingnoteservice.domain.MeetingNote;
import com.sjtu.project.meetingnoteservice.domain.Type;
import com.sjtu.project.meetingnoteservice.global.GlobalAutoWired;
import io.micrometer.core.instrument.util.StringUtils;

public class AddFileMeetingNote extends AddMeetingNoteTransaction {
    String url;

    public AddFileMeetingNote(String title, String meetingId, String owner) {
        super(title, meetingId, owner);
        StringBuilder sb = new StringBuilder();
        sb.append(Constants.bucketname);sb.append(".");
        sb.append(Constants.endpoint);sb.append("/");
        sb.append(title);
        this.url = sb.toString();
    }

    @Override
    protected void generateNoteFor(MeetingNote meetingNote) {
        meetingNote.setNote(this.url);
        GlobalAutoWired.context.getBean(MeetingNoteRepository.class).save(meetingNote);
    }

    @Override
    public void validate() {
        if (StringUtils.isEmpty(url)) {
            throw new IllegalArgumentException("url" + ResultUtil.SPLITTER + "url should not be empty");
        }
    }

    @Override
    protected Type getType() {
        return Type.FILE;
    }
}
