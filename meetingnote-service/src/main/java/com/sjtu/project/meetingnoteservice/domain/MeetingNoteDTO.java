package com.sjtu.project.meetingnoteservice.domain;

import com.sjtu.project.common.transaction.Transaction;
import com.sjtu.project.meetingnoteservice.transactionImpl.AddHtmlMeetingNote;
import com.sjtu.project.meetingnoteservice.transactionImpl.AddVoiceMeetingNote;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class MeetingNoteDTO {

    @NotEmpty
    String title;

    @NotEmpty
    String meetingId;

    @NotEmpty
    String owner;

    String fileName;

    String html;

    Type type;

    public Transaction convert2Transaction() {
        Transaction res;
        switch (type) {
            case HTML:
                res = new AddHtmlMeetingNote(title, meetingId, owner, html);
                break;
            case VOICE:
                res = new AddVoiceMeetingNote(title, meetingId, owner, fileName);
                break;
            case FILE:
            default:
                throw new IllegalArgumentException();
        }
        return res;
    }
}
