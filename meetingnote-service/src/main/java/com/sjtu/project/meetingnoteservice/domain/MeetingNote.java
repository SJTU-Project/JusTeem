package com.sjtu.project.meetingnoteservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document
public class MeetingNote {
    @Id
    String id;

    String title;

    String note;

    String meetingId;

    String owner;

    String meetingHeading;

    Type type;
}
