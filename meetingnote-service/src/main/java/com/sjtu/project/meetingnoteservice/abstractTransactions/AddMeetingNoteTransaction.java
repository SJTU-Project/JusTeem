package com.sjtu.project.meetingnoteservice.abstractTransactions;

import com.sjtu.project.common.transaction.Transaction;
import com.sjtu.project.meetingnoteservice.dao.MeetingNoteRepository;
import com.sjtu.project.meetingnoteservice.domain.MeetingNote;
import com.sjtu.project.meetingnoteservice.domain.Type;
import com.sjtu.project.meetingnoteservice.global.GlobalAutoWired;

public abstract class AddMeetingNoteTransaction implements Transaction {
    String title;

    String meetingId;

    String owner;

    public AddMeetingNoteTransaction(String title, String meetingId, String owner) {
        this.title = title;
        this.meetingId = meetingId;
        this.owner = owner;
    }

    @Override
    public void execute() {
        MeetingNote meetingNote = MeetingNote.builder()
                .id(generateId())
                .title(title)
                .meetingId(meetingId)
                .owner(owner)
                .type(getType())
                .build();
        MeetingNoteRepository repository = GlobalAutoWired.context.getBean(MeetingNoteRepository.class);
        repository.save(meetingNote);
        generateNoteFor(meetingNote);
    }

    protected abstract void generateNoteFor(MeetingNote meetingNote);

    protected String generateId() {
        return null;
    }

    protected abstract Type getType();
}
