package com.sjtu.project.meetingnoteservice.transactionImpl;

import com.sjtu.project.common.util.ResultUtil;
import com.sjtu.project.meetingnoteservice.abstractTransactions.AddMeetingNoteTransaction;
import com.sjtu.project.meetingnoteservice.dao.MeetingNoteRepository;
import com.sjtu.project.meetingnoteservice.domain.MeetingNote;
import com.sjtu.project.meetingnoteservice.domain.Type;
import com.sjtu.project.meetingnoteservice.global.GlobalAutoWired;
import io.micrometer.core.instrument.util.StringUtils;

public class AddHtmlMeetingNote extends AddMeetingNoteTransaction {
    String html;

    public AddHtmlMeetingNote(String title, String meetingId, String owner, String html) {
        super(title, meetingId, owner);
        this.html = html;
    }

    @Override
    protected void generateNoteFor(MeetingNote meetingNote) {
        meetingNote.setNote(this.html);
        GlobalAutoWired.context.getBean(MeetingNoteRepository.class).save(meetingNote);
    }

    @Override
    protected Type getType() {
        return Type.HTML;
    }

    @Override
    public void validate() {
        if (StringUtils.isEmpty(html)) {
            throw new IllegalArgumentException("Html" + ResultUtil.SPLITTER + "Html should not be empty");
        }
    }
}
