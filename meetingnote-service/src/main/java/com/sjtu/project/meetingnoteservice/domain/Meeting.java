package com.sjtu.project.meetingnoteservice.domain;

import com.sjtu.project.common.entity.TimeRange;
import lombok.Data;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class Meeting {
    @Id
    String id;

    @NotBlank
    String heading;

    @NotBlank
    String description;

    @NotBlank
    String date;

    @NotNull
    TimeRange timeRange;

    @NotBlank
    String meetingRoomId;

    @NotBlank
    String meetingRoomLocation;

}
