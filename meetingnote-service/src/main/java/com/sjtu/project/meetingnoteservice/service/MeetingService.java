package com.sjtu.project.meetingnoteservice.service;

import com.google.common.net.HttpHeaders;
import com.sjtu.project.common.response.Result;
import com.sjtu.project.meetingnoteservice.domain.Meeting;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


@FeignClient(value = "meeting-service")
public interface MeetingService {

    @GetMapping("/meeting/{id}")
    public Result<Meeting> findOneMeeting(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable(name = "id") String id);
}

