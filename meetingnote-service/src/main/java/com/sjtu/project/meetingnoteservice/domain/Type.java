package com.sjtu.project.meetingnoteservice.domain;

public enum  Type {
    HTML,
    VOICE,
    FILE
}
