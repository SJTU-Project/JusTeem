package com.sjtu.project.meetingnoteservice.transactionImpl;

import com.sjtu.project.common.util.ResultUtil;
import com.sjtu.project.meetingnoteservice.abstractTransactions.AddMeetingNoteTransaction;
import com.sjtu.project.meetingnoteservice.domain.MeetingNote;
import com.sjtu.project.meetingnoteservice.domain.Type;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class AddVoiceMeetingNote extends AddMeetingNoteTransaction {
    String voiceFileName;

    public AddVoiceMeetingNote(String title, String meetingId, String owner, String fileName) {
        super(title, meetingId, owner);
        this.voiceFileName = fileName;
    }

    @Override
    protected void generateNoteFor(MeetingNote meetingNote) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://47.106.8.44:5000/recognize" +
                "/" +
                meetingNote.getId() +
                "/" +
                voiceFileName;
        log.info("URL: {}", url);
        String res = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(new HttpHeaders()), String.class).getBody();
        log.info("result: {}", res);
    }

    @Override
    public void validate() {
        if (StringUtils.isEmpty(voiceFileName)) {
            throw new IllegalArgumentException("FileName" + ResultUtil.SPLITTER + "FileName should not be empty");
        }
    }

    @Override
    protected Type getType() {
        return Type.VOICE;
    }
}
