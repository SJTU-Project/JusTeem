package com.sjtu.project.meetingnoteservice.dao;

import com.sjtu.project.meetingnoteservice.domain.MeetingNote;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MeetingNoteRepository extends MongoRepository<MeetingNote, String> {
    List<MeetingNote> findAllByMeetingId(String meetingId);

    List<MeetingNote> findAllByOwner(String userId);

    List<MeetingNote> findAllByOwnerIsNot(String userId);

    List<MeetingNote> findAllByOwnerAndMeetingId(String userId, String meetingId);

}
