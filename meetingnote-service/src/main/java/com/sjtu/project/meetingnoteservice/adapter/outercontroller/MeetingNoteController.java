package com.sjtu.project.meetingnoteservice.adapter.outercontroller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.sjtu.project.common.response.Result;
import com.sjtu.project.common.transaction.Transaction;
import com.sjtu.project.common.util.JWTUtil;
import com.sjtu.project.common.util.ResultUtil;
import com.sjtu.project.meetingnoteservice.common.Constants;
import com.sjtu.project.meetingnoteservice.dao.MeetingNoteRepository;
import com.sjtu.project.meetingnoteservice.domain.MeetingNote;
import com.sjtu.project.meetingnoteservice.domain.MeetingNoteDTO;
import com.sjtu.project.meetingnoteservice.service.MeetingService;
import com.sjtu.project.meetingnoteservice.transactionImpl.AddFileMeetingNote;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static com.sjtu.project.common.util.ResultUtil.SPLITTER;

@RestController("outerMeetingNoteController")
@Slf4j
public class MeetingNoteController {
    @Autowired
    MeetingNoteRepository repository;

    @Autowired
    MeetingService meetingService;

    @GetMapping("/meetingNote")
    public Result<List<MeetingNote>> getMeetingNoteByMeetingId(@RequestParam("meetingId") String meetingId) {
        List<MeetingNote> res = repository.findAllByMeetingId(meetingId);
        res = idToHeading(res);
        return ResultUtil.success(res);
    }

    @DeleteMapping("/meetingNote")
    public Result deleteMeetingNoteById(@RequestParam("noteId") String noteId){
        repository.deleteById(noteId);
        return ResultUtil.success();
    }

    @PostMapping("/meetingNote")
    public Result addMeetingNoe(@RequestBody @Validated MeetingNoteDTO meetingNoteDTO) {
        Transaction transaction = meetingNoteDTO.convert2Transaction();
        transaction.validate();
        transaction.execute();
        return ResultUtil.success();
    }

    @PostMapping("/meetingNote/file")
    public Result addFileMeetingNote(@RequestParam("file") MultipartFile file,
                                     @RequestParam("title") String title,
                                     @RequestParam("meetingId") String meetingId,
                                     @RequestParam("owner") String owner) {
        OSS ossClient = new OSSClientBuilder().build(Constants.endpoint, Constants.id, Constants.key);

        try {
            ossClient.putObject(Constants.bucketname, title, file.getInputStream());
            ossClient.shutdown();
        }
        catch (IOException e) {
            log.error("", e);
            throw new IllegalArgumentException("File" + SPLITTER + "Illegal File");
        }
        Transaction transaction = new AddFileMeetingNote(title, meetingId, owner);
        transaction.validate();
        transaction.execute();
        return ResultUtil.success();
    }

    @PutMapping("/meetingNote")
    public Result<MeetingNote> modifyNote(@RequestBody MeetingNote note){
        return ResultUtil.success(repository.save(note));
    }

    /**
     * frontend api
     */
    @GetMapping("/meetingNote/userId/meetingId")
    public Result<MeetingNote> getMeetingNoteByUserIdByMeetingId(@RequestParam("userId") String userId,
                                                            @RequestParam("meetingId") String meetingId) {
        List<MeetingNote> res = repository.findAllByOwnerAndMeetingId(userId, meetingId);
        res = idToHeading(res);
        return ResultUtil.success(res.size() == 0 ? null : res.get(0));
    }


    @GetMapping("/meetingNote/userId")
    public Result<List<MeetingNote>> getMeetingNoteByUserId(@RequestParam("userId") String userId,
                                                            @RequestParam("own") Boolean own) {
        List<MeetingNote> res = own ? repository.findAllByOwner(userId) :
                repository.findAllByOwnerIsNot(userId);
        res = idToHeading(res);
        return ResultUtil.success(res);
    }

    private List<MeetingNote> idToHeading(List<MeetingNote> notes){
        notes.forEach(note -> {
            note.setMeetingHeading(meetingService.findOneMeeting(JWTUtil.adminToken, note.getMeetingId()).getData().getHeading());
        });
        return notes;
    }
}
