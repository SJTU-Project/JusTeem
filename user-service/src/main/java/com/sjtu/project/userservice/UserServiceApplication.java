package com.sjtu.project.userservice;

import com.sjtu.project.common.annotation.JusTeemApplication;
import org.springframework.boot.SpringApplication;

@JusTeemApplication
public class UserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }

}
