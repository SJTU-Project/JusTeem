package com.sjtu.project.meetingroomservice.adapter.interncontroller;

import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.common.response.Result;
import com.sjtu.project.common.util.ResultUtil;
import com.sjtu.project.meetingroomservice.domain.Requirement;
import com.sjtu.project.meetingroomservice.domain.ScheduleDTO;
import com.sjtu.project.meetingroomservice.service.MeetingRoomService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController("InternMeetingRoomController")
@Slf4j
public class MeetingRoomController {
    @Autowired
    MeetingRoomService meetingRoomService;

    @PostMapping("/schedule/query")
    public Result<ScheduleDTO> getMeetingRoomConditional(@RequestBody Requirement requirement) {
        log.info("start query with {}", requirement);
        return ResultUtil.success(meetingRoomService.findScheduleByCondition(requirement));
    }

    @PutMapping("/meetingroom/{id}/schedule/{date}/timeSlice")
    public Result modifySchedule(@PathVariable String id,
                                 @PathVariable String date,
                                 @RequestParam(name = "occupied") boolean occupied,
                                 @RequestBody TimeRange timeRange) {
        meetingRoomService.modifySchedule(id, date, timeRange, occupied);
        return ResultUtil.success();
    }
}
