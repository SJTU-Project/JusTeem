package com.sjtu.project.meetingroomservice.domain;

import com.sjtu.project.common.entity.TimeRange;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TimeSlice {
    public static List<TimeSlice> timeSliceList = new ArrayList<>();

    static {
        for (TimeRange timeRange : TimeRange.timeRangeList) {
            TimeSlice timeSlice = new TimeSlice();
            timeSlice.setTimeRange(timeRange);
            timeSliceList.add(timeSlice);
        }
    }

    TimeRange timeRange;

    boolean isOccupied = false;
}
