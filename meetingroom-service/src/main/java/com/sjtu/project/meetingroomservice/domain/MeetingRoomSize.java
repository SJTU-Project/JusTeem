package com.sjtu.project.meetingroomservice.domain;

public enum MeetingRoomSize {
    BIG,
    MIDDLE,
    SMALL
}
