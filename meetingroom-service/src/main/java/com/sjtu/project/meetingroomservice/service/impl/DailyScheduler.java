package com.sjtu.project.meetingroomservice.service.impl;

import com.sjtu.project.common.exception.ObjectNotFoundException;
import com.sjtu.project.meetingroomservice.dao.OneDayScheduleRepository;
import com.sjtu.project.meetingroomservice.domain.MeetingRoom;
import com.sjtu.project.meetingroomservice.service.MeetingRoomService;
import com.sjtu.project.meetingroomservice.service.ScheduleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.redis.util.RedisLockRegistry;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.locks.Lock;

@Component
@Slf4j
public class DailyScheduler {
    @Autowired
    RedisLockRegistry redisLockRegistry;

    @Autowired
    ScheduleService scheduleService;

    @Autowired
    OneDayScheduleRepository oneDayScheduleRepository;

    @Autowired
    MeetingRoomService meetingRoomService;

    @Scheduled(cron = "0 0 1 * * *")
    public void addScheduleForMeetingRoom() {
        Date addedDate = DateUtils.addDays(new Date(), 7);
        log.info("Scheduler start!");
        meetingRoomService.findAll().parallelStream()
                .map(MeetingRoom::getId)
                .forEach((id) -> {
                    Lock lock = redisLockRegistry.obtain(id);
                    lock.lock();
                    try {
                        scheduleService.getOneDaySchedule(id, addedDate);
                    }
                    catch (ObjectNotFoundException e) {
                        scheduleService.createOneDaySchedule(id, addedDate);
                    }
                    finally {
                        lock.unlock();
                    }
                });
    }
}
