package com.sjtu.project.meetingroomservice.domain;

import com.sjtu.project.common.entity.ImageURL;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MeetingRoomVO {
    String id;

    MeetingRoomSize size = MeetingRoomSize.SMALL;

    String location = "";

    String description = "";

    boolean existed = true;

    List<MeetingRoomDevice> deviceList = new ArrayList<>();

    List<ImageURL> images = new ArrayList<>();

    List<OneDaySchedule> oneWeekSchedules = new ArrayList<>();
}
