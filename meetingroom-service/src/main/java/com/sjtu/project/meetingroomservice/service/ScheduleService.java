package com.sjtu.project.meetingroomservice.service;

import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.meetingroomservice.domain.OneDaySchedule;
import com.sjtu.project.meetingroomservice.domain.ScheduleDTO;

import java.util.Date;
import java.util.List;

public interface ScheduleService {
    void createOneDaySchedule(String meetingRoomId, Date date);

    void createOneWeekSchedules(String meetingRoomId, Date week);

    OneDaySchedule getOneDaySchedule(String meetingRoomId, Date date);

    List<OneDaySchedule> getOneWeekSchedules(String meetingRoomId, Date week);

    void modifySchedule(String id, String date, TimeRange timeRange, boolean occupied);

    ScheduleDTO findSuitableSchedule(String id, String date, TimeRange timeRange);
}
