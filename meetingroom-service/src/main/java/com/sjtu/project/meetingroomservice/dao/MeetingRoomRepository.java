package com.sjtu.project.meetingroomservice.dao;

import com.sjtu.project.meetingroomservice.domain.MeetingRoom;
import com.sjtu.project.meetingroomservice.domain.MeetingRoomDevice;
import com.sjtu.project.meetingroomservice.domain.MeetingRoomSize;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeetingRoomRepository extends MongoRepository<MeetingRoom, String>, CustomMeetingRoomRepo {
    Page<MeetingRoom> findAllByExistedIsTrue(PageRequest pageRequest);

    MeetingRoom findOneByIdAndExistedIsTrue(String id);

    List<MeetingRoom> findAllByExistedIsTrue();

    List<MeetingRoom> findAllByExistedIsTrueAndDeviceListContains(List<MeetingRoomDevice> meetingRoomDevices);

    List<MeetingRoom> findAllByExistedIsTrueAndDeviceListContainsAndSize(List<MeetingRoomDevice> meetingRoomDevices, MeetingRoomSize size);
}
