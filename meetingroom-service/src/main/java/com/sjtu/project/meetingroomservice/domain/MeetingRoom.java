package com.sjtu.project.meetingroomservice.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sjtu.project.common.entity.ImageURL;
import lombok.Data;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document
@Data
public class MeetingRoom {
    @Id
    String id;

    MeetingRoomSize size = MeetingRoomSize.SMALL;

    @Indexed(unique = true)
    @NotBlank
    String location = "";

    @NotBlank
    String description = "";

    boolean existed = true;

    List<MeetingRoomDevice> deviceList = new ArrayList<>();

    List<ImageURL> images = new ArrayList<>();

    @CreatedBy
    String creator;

    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
    Date createTime;

    @LastModifiedBy
    String modifier;

    @LastModifiedDate
    @JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
    Date lastModifiedTime;
}
