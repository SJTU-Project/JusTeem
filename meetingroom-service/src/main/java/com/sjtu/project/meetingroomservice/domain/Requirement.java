package com.sjtu.project.meetingroomservice.domain;

import com.sjtu.project.common.entity.TimeRange;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Requirement {
    String date;

    TimeRange timeRange;

    MeetingRoomSize size;

    List<MeetingRoomDevice> meetingRoomDeviceList = new ArrayList<>();

    String location;
}
