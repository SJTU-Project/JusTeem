package com.sjtu.project.meetingroomservice.service.impl;

import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.common.exception.ObjectNotFoundException;
import com.sjtu.project.common.util.DateUtil;
import com.sjtu.project.common.util.ResultUtil;
import com.sjtu.project.meetingroomservice.dao.OneDayScheduleRepository;
import com.sjtu.project.meetingroomservice.domain.MeetingRoom;
import com.sjtu.project.meetingroomservice.domain.OneDaySchedule;
import com.sjtu.project.meetingroomservice.domain.ScheduleDTO;
import com.sjtu.project.meetingroomservice.domain.TimeSlice;
import com.sjtu.project.meetingroomservice.service.MeetingRoomService;
import com.sjtu.project.meetingroomservice.service.ScheduleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.redis.util.RedisLockRegistry;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.locks.Lock;

@Service
@Slf4j
public class ScheduleServiceImpl implements ScheduleService {
    @Autowired
    MeetingRoomService meetingRoomService;

    @Autowired
    OneDayScheduleRepository oneDayScheduleRepository;

    @Autowired
    RedisLockRegistry redisLockRegistry;

    @PostConstruct
    public void init() {
        meetingRoomService.findAll()
                .stream()
                .map(MeetingRoom::getId)
                .forEach(this::fixWrongSchedule);
    }

    private void fixWrongSchedule(String meetingRoomId) {
        for (Date date : DateUtil.getOneWeekDates(new Date())) {
            OneDaySchedule oneDaySchedule = oneDayScheduleRepository.findOneByMeetingRoomIdAndDate(meetingRoomId, DateUtil.convert2String(date));
            if (oneDaySchedule == null) {
                createOneDaySchedule(meetingRoomId, date);
            }
        }
    }

    @Override
    public void createOneDaySchedule(String meetingRoomId, Date date) {
        if (meetingRoomId == null || meetingRoomId.equals("")) {
            throw new IllegalArgumentException("meetingRoomId" + ResultUtil.SPLITTER + "meetingRoomId should not be empty");
        }
        OneDaySchedule oneDaySchedule = new OneDaySchedule()
                .withMeetingRoomId(meetingRoomId)
                .withDate(DateUtil.convert2String(date))
                .withTimeSliceList(TimeSlice.timeSliceList);
        oneDayScheduleRepository.save(oneDaySchedule);
    }

    @Override
    public void createOneWeekSchedules(String meetingRoomId, Date week) {
        if (meetingRoomId == null || meetingRoomId.equals("")) {
            throw new IllegalArgumentException("meetingRoomId" + ResultUtil.SPLITTER + "meetingRoomId should not be empty");
        }
        for (Date date : DateUtil.getOneWeekDates(new Date())) {
            createOneDaySchedule(meetingRoomId, date);
        }
    }

    @Override
    public OneDaySchedule getOneDaySchedule(String meetingRoomId, Date date) {
        return getOneDaySchedule(meetingRoomId, DateUtil.convert2String(date));
    }

    private OneDaySchedule getOneDaySchedule(String meetingRoomId, String date) {
        if (meetingRoomId == null || meetingRoomId.equals("")) {
            throw new IllegalArgumentException("MeetingRoomId" + ResultUtil.SPLITTER + "MeetingRoomId should not be empty");
        }
        OneDaySchedule res = oneDayScheduleRepository.findOneByMeetingRoomIdAndDate(meetingRoomId, date);
        if (res == null) {
            throw new ObjectNotFoundException("OneDaySchedule");
        }
        else {
            return res;
        }

    }

    @Override
    public List<OneDaySchedule> getOneWeekSchedules(String meetingRoomId, Date week) {
        if (meetingRoomId == null || meetingRoomId.equals("")) {
            throw new IllegalArgumentException("MeetingRoomId" + ResultUtil.SPLITTER + "MeetingRoomId should not be empty");
        }
        List<OneDaySchedule> res = new ArrayList<>();
        for (Date date : DateUtil.getOneWeekDates(week)) {
            res.add(getOneDaySchedule(meetingRoomId, date));
        }
        return res;
    }

    @Override
    public void modifySchedule(String id, String date, TimeRange timeRange, boolean occupied) {
        Lock lock = redisLockRegistry.obtain(id + date);
        try{
            lock.lock();
            OneDaySchedule oneDaySchedule = getOneDaySchedule(id, date);
            oneDaySchedule.modifyTimeRange(timeRange, occupied);
            oneDayScheduleRepository.save(oneDaySchedule);
        }
        finally {
            lock.unlock();
        }
    }

    @Override
    public ScheduleDTO findSuitableSchedule(String id, String date, TimeRange timeRange) {
        List<OneDaySchedule> schedules = StringUtils.isEmpty(date)
                ? getOneWeekSchedules(id, new Date())
                : Arrays.asList(getOneDaySchedule(id, date));
        for (OneDaySchedule schedule : schedules) {
            TimeRange suitableTimeRange = schedule.findSuitableTimeRange(timeRange);
            if (suitableTimeRange != null) {
                return new ScheduleDTO().withDate(schedule.getDate())
                                        .withTimeRange(suitableTimeRange);
            }
        }
        return null;
    }
}
