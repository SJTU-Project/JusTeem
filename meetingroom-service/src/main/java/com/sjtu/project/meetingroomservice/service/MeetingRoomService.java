package com.sjtu.project.meetingroomservice.service;

import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.common.response.PageResult;
import com.sjtu.project.meetingroomservice.domain.MeetingRoom;
import com.sjtu.project.meetingroomservice.domain.MeetingRoomVO;
import com.sjtu.project.meetingroomservice.domain.Requirement;
import com.sjtu.project.meetingroomservice.domain.ScheduleDTO;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface MeetingRoomService {
    PageResult<MeetingRoom> findAll(PageRequest pageRequest);

    List<MeetingRoom> findAll();

    MeetingRoom findOneById(String id);

    MeetingRoomVO convert2VO(MeetingRoom meetingRoom);

    MeetingRoom deleteOneById(String id);

    MeetingRoom add(MeetingRoom meetingRoom);

    MeetingRoom update(String id, MeetingRoom meetingRoom);

    void modifySchedule(String id, String date, TimeRange timeRange, boolean occupied);

    List<MeetingRoom> findMeetingRoomByCondition(Requirement requirement);

    ScheduleDTO findScheduleByCondition(Requirement requirement);
}
