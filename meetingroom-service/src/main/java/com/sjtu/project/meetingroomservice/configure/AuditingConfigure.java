package com.sjtu.project.meetingroomservice.configure;

import com.sjtu.project.common.audit.UserAuditing;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@Configuration
@EnableMongoAuditing
public class AuditingConfigure {
    @Bean
    public AuditorAware<String> auditorAware() {
        return new UserAuditing();
    }
}
