package com.sjtu.project.meetingroomservice.adapter.outercontroller;

import com.sjtu.project.common.response.PageResult;
import com.sjtu.project.common.response.Result;
import com.sjtu.project.common.util.ResultUtil;
import com.sjtu.project.meetingroomservice.domain.MeetingRoom;
import com.sjtu.project.meetingroomservice.domain.MeetingRoomVO;
import com.sjtu.project.meetingroomservice.domain.Requirement;
import com.sjtu.project.meetingroomservice.service.MeetingRoomService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class MeetingRoomController {
    @Autowired
    MeetingRoomService meetingRoomService;

    @GetMapping("/meetingroom")
    public Result<PageResult<MeetingRoom>> getMeetingRooms(@RequestParam(name = "pageNum", defaultValue = "1000") Integer pageNum,
                                                          @RequestParam(name = "pageIndex", defaultValue = "0") Integer pageIndex) {
        return ResultUtil.success(meetingRoomService.findAll(PageRequest.of(pageIndex, pageNum)));
    }

    @PostMapping("/meetingroom")
    public Result<MeetingRoom> addMeetingRoom(@Valid @RequestBody MeetingRoom meetingRoom) {
        return ResultUtil.success(meetingRoomService.add(meetingRoom));
    }

    @GetMapping("/meetingroom/{id}")
    public Result<MeetingRoomVO>  getMeetingRoom(@PathVariable String id) {
        MeetingRoom meetingRoom = meetingRoomService.findOneById(id);
        return ResultUtil.success(meetingRoomService.convert2VO(meetingRoom));
    }

    @GetMapping("/meetingroom/condition")
    public Result<List<MeetingRoom>>  getMeetingRoomByCondition(Requirement requirement) {
        return ResultUtil.success(meetingRoomService.findMeetingRoomByCondition(requirement));
    }

    @PutMapping("/meetingroom/{id}")
    public Result<MeetingRoom> updateMeetingRoom(@PathVariable String id,
                                                 @RequestBody MeetingRoom meetingRoom) {
        return ResultUtil.success(meetingRoomService.update(id, meetingRoom));
    }

    @DeleteMapping("/meetingroom/{id}")
    public Result<MeetingRoom> deleteMeetingRoom(@PathVariable String id) {
        return ResultUtil.success(meetingRoomService.deleteOneById(id));
    }
}
