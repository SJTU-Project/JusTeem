package com.sjtu.project.meetingroomservice.dao;

import com.sjtu.project.meetingroomservice.domain.OneDaySchedule;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OneDayScheduleRepository extends MongoRepository<OneDaySchedule, String> {
    OneDaySchedule findOneByMeetingRoomIdAndDate(String meetingRoomId, String date);
}
