package com.sjtu.project.meetingroomservice.dao;

import com.sjtu.project.meetingroomservice.domain.MeetingRoom;
import com.sjtu.project.meetingroomservice.domain.MeetingRoomDevice;
import com.sjtu.project.meetingroomservice.domain.MeetingRoomSize;
import com.sjtu.project.meetingroomservice.domain.Requirement;

import java.util.List;

public interface CustomMeetingRoomRepo {
    List<MeetingRoom> findByCondition(Requirement requirement);
}
