package com.sjtu.project.meetingroomservice.domain;

import lombok.Getter;

public enum MeetingRoomDevice {
    AIRCONDITIONER("空调"),
    BLACKBOARD("黑板"),
    TABLE("桌子"),
    PROJECTOR("投影仪"),
    POWERSUPPLY("电源"),
    WIRELESS("无线网络"),
    NETWORK("有线网络"),
    TV("电视");

    @Getter
    String name;

    MeetingRoomDevice(String name) {
        this.name = name;
    }
}
