package com.sjtu.project.meetingroomservice.service.impl;

import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.common.exception.ObjectNotFoundException;
import com.sjtu.project.common.exception.ResourceNotMatchException;
import com.sjtu.project.common.response.PageResult;
import com.sjtu.project.common.util.PageUtil;
import com.sjtu.project.meetingroomservice.dao.MeetingRoomRepository;
import com.sjtu.project.meetingroomservice.domain.MeetingRoom;
import com.sjtu.project.meetingroomservice.domain.MeetingRoomVO;
import com.sjtu.project.meetingroomservice.domain.Requirement;
import com.sjtu.project.meetingroomservice.domain.ScheduleDTO;
import com.sjtu.project.meetingroomservice.service.MeetingRoomService;
import com.sjtu.project.meetingroomservice.service.ScheduleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class MeetingRoomServiceImpl implements MeetingRoomService {
    @Autowired
    MeetingRoomRepository meetingRoomRepository;

    @Autowired
    ScheduleService scheduleService;

    @Override
    public PageResult<MeetingRoom> findAll(PageRequest pageRequest) {
        return PageUtil.convert2PageResult(meetingRoomRepository.findAllByExistedIsTrue(pageRequest));
    }

    @Override
    public List<MeetingRoom> findAll() {
        return meetingRoomRepository.findAllByExistedIsTrue();
    }

    @Override
    public MeetingRoom findOneById(String id) {
        MeetingRoom res = meetingRoomRepository.findOneByIdAndExistedIsTrue(id);
        if (res == null) {
            throw new ObjectNotFoundException("meetingroom");
        }
        else return res;
    }

    @Override
    public List<MeetingRoom> findMeetingRoomByCondition(Requirement requirement) {
        return meetingRoomRepository.findByCondition(requirement);
    }

    @Override
    public ScheduleDTO findScheduleByCondition(Requirement requirement) {
        List<MeetingRoom> meetingRooms = findMeetingRoomByCondition(requirement);
        for (MeetingRoom meetingRoom : meetingRooms) {
            ScheduleDTO res = scheduleService.findSuitableSchedule(meetingRoom.getId(), requirement.getDate(), requirement.getTimeRange());
            if (res != null) {
                return res.withMeetingRoom(meetingRoom);
            }
        }
        return null;
    }

    @Override
    public MeetingRoomVO convert2VO(MeetingRoom meetingRoom) {
        MeetingRoomVO res = new MeetingRoomVO();
        BeanUtils.copyProperties(meetingRoom, res);
        res.setOneWeekSchedules(scheduleService.getOneWeekSchedules(res.getId(), new Date()));
        return res;
    }

    @Override
    public MeetingRoom deleteOneById(String id) {
        MeetingRoom res = findOneById(id);
        res.setExisted(false);
        res.setLocation(res.getLocation() + "-" + UUID.randomUUID().toString());
        return meetingRoomRepository.save(res);
    }

    @Override
    @Transactional
    public MeetingRoom add(MeetingRoom meetingRoom) {
        meetingRoom.setId(null);
        meetingRoom.setExisted(true);
        meetingRoomRepository.save(meetingRoom);
        scheduleService.createOneWeekSchedules(meetingRoom.getId(), new Date());
        return meetingRoom;
    }

    @Override
    public MeetingRoom update(String id, MeetingRoom meetingRoom) {
        if (!id.equals(meetingRoom.getId())) {
            throw new ResourceNotMatchException("meetingroom");
        }
        MeetingRoom oldMeetingRoom = findOneById(id);
        oldMeetingRoom.setSize(meetingRoom.getSize());
        oldMeetingRoom.setLocation(meetingRoom.getLocation());
        oldMeetingRoom.setImages(meetingRoom.getImages());
        oldMeetingRoom.setDeviceList(meetingRoom.getDeviceList());
        return meetingRoomRepository.save(oldMeetingRoom);
    }

    @Override
    public void modifySchedule(String id, String date, TimeRange timeRange, boolean occupied) {
        scheduleService.modifySchedule(id, date, timeRange, occupied);
    }
}
