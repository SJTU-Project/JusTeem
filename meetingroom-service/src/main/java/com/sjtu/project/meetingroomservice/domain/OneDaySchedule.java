package com.sjtu.project.meetingroomservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sjtu.project.common.entity.TimeRange;
import com.sjtu.project.common.exception.TimeRangeNotOccupiedException;
import com.sjtu.project.common.exception.TimeRangeOccupiedException;
import com.sjtu.project.common.util.DateUtil;
import com.sjtu.project.common.util.TimeRangeUtil;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document
@Data
public class OneDaySchedule {
    @Id
    @JsonIgnore
    String id;

    @JsonIgnore
    String meetingRoomId;

    String date;

    List<TimeSlice> timeSliceList;

    public OneDaySchedule withMeetingRoomId(String meetingRoomId) {
        this.meetingRoomId = meetingRoomId;
        return this;
    }

    public OneDaySchedule withDate(String date) {
        this.date = date;
        return this;
    }

    public OneDaySchedule withTimeSliceList(List<TimeSlice> timeSliceList) {
        this.timeSliceList = timeSliceList;
        return this;
    }

    public void modifyTimeRange(TimeRange timeRange, boolean occupied) {
        List<TimeSlice> tmp = new ArrayList<>();
        for (int dateNumber = timeRange.getStart(); dateNumber < timeRange.getEnd(); ++dateNumber) {
            int index = TimeRangeUtil.convertDateNumber2Index(dateNumber);
            TimeSlice timeSlice = timeSliceList.get(index);
            if (occupied && timeSlice.isOccupied()) {
                throw new TimeRangeOccupiedException(TimeRangeUtil.convertDateNumber2String(dateNumber));
            }
            else if (!occupied && !timeSlice.isOccupied()) {
                throw new TimeRangeNotOccupiedException(TimeRangeUtil.convertDateNumber2String(dateNumber));
            }
            else tmp.add(timeSlice);
        }
        for (TimeSlice timeSlice : tmp) {
            timeSlice.setOccupied(occupied);
        }
    }

    public TimeRange findSuitableTimeRange(TimeRange timeRange) {
        if (timeRange == null) {
            for (TimeSlice timeSlice : timeSliceList) {
                if (!timeSlice.isOccupied && isStartTimeBeforeNow(this.date, timeSlice.getTimeRange())) {
                    return timeSlice.getTimeRange();
                }
            }
            return null;
        }
        else {
            TimeRangeUtil.validateTimeRange(timeRange);
            if (isStartTimeBeforeNow(this.date, timeRange)) {
                return null;
            }
            for (int dateNumber = timeRange.getStart(); dateNumber < timeRange.getEnd(); ++dateNumber) {
                int index = TimeRangeUtil.convertDateNumber2Index(dateNumber);
                TimeSlice timeSlice = timeSliceList.get(index);
                if (timeSlice.isOccupied()) {
                    return null;
                }
            }
            return timeRange;
        }
    }

    private boolean isStartTimeBeforeNow(String date, TimeRange timeRange) {
        String time = date + " " + TimeRangeUtil.convertDateNumber2String(timeRange.getStart());
        Date startDate = DateUtil.convertTime2Date(time);
        return startDate.before(new Date());
    }
}
