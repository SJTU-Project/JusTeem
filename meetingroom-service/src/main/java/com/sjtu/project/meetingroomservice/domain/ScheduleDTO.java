package com.sjtu.project.meetingroomservice.domain;

import com.sjtu.project.common.entity.TimeRange;
import lombok.Data;

@Data
public class ScheduleDTO {
    String date;

    TimeRange timeRange;

    String meetingRoomId;

    String meetingRoomLocation;

    public ScheduleDTO withDate(String date) {
        this.date = date;
        return this;
    }

    public ScheduleDTO withTimeRange(TimeRange timeRange) {
        this.timeRange = timeRange;
        return this;
    }

    public ScheduleDTO withMeetingRoom(MeetingRoom meetingRoom) {
        this.meetingRoomId = meetingRoom.getId();
        this.meetingRoomLocation = meetingRoom.getLocation();
        return this;
    }
}
