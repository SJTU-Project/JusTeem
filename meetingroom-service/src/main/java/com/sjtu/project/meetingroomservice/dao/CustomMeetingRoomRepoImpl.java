package com.sjtu.project.meetingroomservice.dao;

import com.sjtu.project.meetingroomservice.domain.MeetingRoom;
import com.sjtu.project.meetingroomservice.domain.MeetingRoomDevice;
import com.sjtu.project.meetingroomservice.domain.MeetingRoomSize;
import com.sjtu.project.meetingroomservice.domain.Requirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomMeetingRoomRepoImpl implements CustomMeetingRoomRepo{
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<MeetingRoom> findByCondition(Requirement requirement) {
        List<MeetingRoomDevice> deviceList = requirement.getMeetingRoomDeviceList();
        MeetingRoomSize size = requirement.getSize();
        String location = requirement.getLocation();

        Query query = new Query();
        query.addCriteria((Criteria.where("existed").is(true)));
        if (deviceList != null && deviceList.size() != 0)
            query.addCriteria(Criteria.where("deviceList").all(deviceList));
        if (size != null)
            query.addCriteria(Criteria.where("size").is(size));
        if (location != null)
            query.addCriteria(Criteria.where("location").regex(".*"+location+".*"));
        return mongoTemplate.find(query, MeetingRoom.class);
    }
}
