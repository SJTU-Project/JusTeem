package com.sjtu.project.meetingroomservice;

import com.sjtu.project.common.annotation.JusTeemApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@JusTeemApplication
@EnableScheduling
public class MeetingRoomServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MeetingRoomServiceApplication.class, args);
    }

}
