package com.sjtu.project.Controller;

import com.sjtu.project.Assembler.CommandAssembler;
import com.sjtu.project.DTO.CommandParameter.OperationParameter;
import com.sjtu.project.Domain.SessionEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint(value = "/websocket/{id}")
@Component
public class WebsocketEndpoint {

    public static final Map<String, SessionEntity> sessionMap = new ConcurrentHashMap<>();
    private static final Logger logger = LoggerFactory.getLogger(WebsocketEndpoint.class);

    @OnOpen
    public void onOpen(Session session, @PathParam("id") String id) {
        //link test
        OperationParameter op = new OperationParameter();
        op.setDevice("Projection");
        op.setOperation("OPEN");
        String linkTest = CommandAssembler.createOperationCommand(op).toString();
        logger.info(linkTest);
        session.getAsyncRemote().sendText(linkTest);

        if (sessionMap.containsKey(id)){
            String warning = String.format("Connection of device %s has been established", id);
            logger.info(warning);
            session.getAsyncRemote().sendText(warning);
        }
        logger.info(String.format("Connection of device %s is established", id));
        SessionEntity se = new SessionEntity(session, id);
        sessionMap.put(id, se);
    }

    @OnClose
    public void onClose(Session session, @PathParam("id") String id) {
        logger.info(String.format("Connection of device %s is closed", id));
        sessionMap.remove(id);
    }

    @OnMessage
    public void onMessage(String message, Session session, @PathParam("id") String id) {
        //TODO
        logger.info("Get Message From Terminal: " + message);
    }

    @OnError
    public void onError(Session session, Throwable error, @PathParam("id") String id) {
        error.printStackTrace();
        session.getAsyncRemote().sendText(error.getMessage());

    }

}
