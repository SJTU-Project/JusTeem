package com.sjtu.project.Controller;

import com.sjtu.project.Assembler.CommandAssembler;
import com.sjtu.project.DTO.CommandParameter.OperationParameter;
import com.sjtu.project.Domain.SessionEntity;
import com.sjtu.project.common.response.Result;
import com.sjtu.project.common.response.ResultCode;
import com.sjtu.project.common.util.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class IotController {
    @Autowired
    WebsocketEndpoint websocketEndpoint;

    @PostMapping("/operation/{meetingRoomId}")
    public Result<String> operateDevice(@PathVariable String meetingRoomId, @RequestBody List<OperationParameter> params) {
        SessionEntity device = WebsocketEndpoint.sessionMap.get(meetingRoomId);
        log.info("DEBUG : 11111 Get HERE!");
        if (device == null) {
            return ResultUtil.failure(ResultCode.FAILURE);
        } else {
            for (OperationParameter operation : params)
                device.sendAsync(CommandAssembler.createOperationCommand(operation).toString());
        }
        return ResultUtil.success("Requested operation for device %s is sent");
    }

    @PostMapping("/operation/{meetingRoomId}/{device}/{operation}")
    public Result<String> setDevice(@PathVariable String meetingRoomId, @PathVariable String device, @PathVariable String operation) {
        //TODO
        SessionEntity deviceId = WebsocketEndpoint.sessionMap.get(meetingRoomId);
        log.info("DEBUG : 22222 Get HERE!");
        if (deviceId == null) {
            return ResultUtil.failure(ResultCode.FAILURE);
        } else {
            OperationParameter param = new OperationParameter();
            param.setOperation(operation);
            param.setDevice(device);
            deviceId.sendAsync(CommandAssembler.createOperationCommand(param).toString());
        }
        return ResultUtil.success("Requested operation for device is sent");
    }
}
