package com.sjtu.project.DTO;

import com.alibaba.fastjson.JSON;
import com.sjtu.project.DTO.CommandParameter.Parameter;

public class CommandDTO {
    public static final String OPERATION = "OPERATION";

    private String action;
    private Parameter parameter;

    public CommandDTO(String action, Parameter parameter) {
        this.action = action;
        this.parameter = parameter;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Parameter getParameter() {
        return parameter;
    }

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
