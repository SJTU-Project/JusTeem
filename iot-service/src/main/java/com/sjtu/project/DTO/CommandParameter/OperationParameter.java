package com.sjtu.project.DTO.CommandParameter;

public class OperationParameter extends Parameter {
    private String device;
    private String operation;

    public OperationParameter(){}

    public OperationParameter(String device, String operation) {
        this.device = device;
        this.operation = operation;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public OperationParameter withDevice(String device){
        this.device = device;
        return this;
    }

    public OperationParameter withOperation(String operation){
        this.operation = operation;
        return this;
    }
}
