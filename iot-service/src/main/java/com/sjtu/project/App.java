package com.sjtu.project;

import com.sjtu.project.common.annotation.JusTeemApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

@JusTeemApplication
@EnableWebSocket
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
