package com.sjtu.project.Assembler;

import com.sjtu.project.DTO.ResultDTO;

public class ResultAssembler {
    public static ResultDTO createSuccessMessage(String message){
        return new ResultDTO(ResultDTO.SUCCESS, message);
    }

    public static ResultDTO createErrorMessage(String message){
        return new ResultDTO(ResultDTO.ERROR, message);
    }
}
