package com.sjtu.project.Assembler;

import com.sjtu.project.DTO.CommandParameter.OperationParameter;
import com.sjtu.project.DTO.CommandDTO;

public class CommandAssembler {
    public static CommandDTO createOperationCommand(OperationParameter param){
        return new CommandDTO(CommandDTO.OPERATION, param);
    }
}
