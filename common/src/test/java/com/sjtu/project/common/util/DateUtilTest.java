package com.sjtu.project.common.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.List;

public class DateUtilTest {

    @Test
    public void testGetOneWeekDates() {
        Date now = new Date();
        List<Date> dates = DateUtil.getOneWeekDates(now);
        Assert.assertEquals(7, dates.size());
        for (int i=0; i < 6; ++i) {
            int intervalDay = (int) ((dates.get(i + 1).getTime() - dates.get(i).getTime()) / (1000 * 60 * 60 * 24));
            Assert.assertEquals(1, intervalDay);
        }
    }

    @Test
    public void testConvertTime2Date() {
        Date dateBefore = DateUtil.convertTime2Date("2019-10-28 16:14");
        Assert.assertTrue(dateBefore.before(new Date()));
    }

    @Test
    public void testIsValidDate() {
        DateUtil.validateDate("2019-05-25");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsValidDateFalse() {
        DateUtil.validateDate("string");
    }
}