package com.sjtu.project.common.util;

import org.junit.Assert;
import org.junit.Test;

public class TimeRangeUtilTest {

    @Test
    public void testConvertDateNumber2String() {
        String time = TimeRangeUtil.convertDateNumber2String(22);
        Assert.assertEquals("11:00", time);
    }

    @Test
    public void testConvertDateNumber2String2() {
        String time = TimeRangeUtil.convertDateNumber2String(16);
        Assert.assertEquals("08:00", time);
    }
}