package com.sjtu.project.common.util;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ResultUtilTest {
    @Test
    public void testSplitter() {
        String message = "abc" + ResultUtil.SPLITTER + "def";
        String[] messages = message.split(ResultUtil.SPLITTER);
        Assert.assertEquals("abc", messages[0]);
    }
}