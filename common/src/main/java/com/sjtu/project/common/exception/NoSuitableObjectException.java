package com.sjtu.project.common.exception;

public class NoSuitableObjectException extends RuntimeException{
    public NoSuitableObjectException() {
        super();
    }

    public NoSuitableObjectException(String message) {
        super(message);
    }
}
