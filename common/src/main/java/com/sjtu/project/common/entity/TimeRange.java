package com.sjtu.project.common.entity;

import com.sjtu.project.common.util.TimeRangeUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TimeRange {
    public static List<TimeRange> timeRangeList = new ArrayList<>();

    static {
        for (int i = 0; i< TimeRangeUtil.ONE_DAY_TIME_RANGE_SIZE; ++i) {
            TimeRange timeRange = new TimeRange();
            timeRange.setStart(TimeRangeUtil.convertIndex2DateNumber(i));
            timeRange.setEnd(TimeRangeUtil.convertIndex2DateNumber(i) + 1);
            timeRangeList.add(timeRange);
        }
    }
    int start;

    int end;

    public static List<TimeRange> getTimeRangeList() {
        return timeRangeList;
    }

    public static void setTimeRangeList(List<TimeRange> timeRangeList) {
        TimeRange.timeRangeList = timeRangeList;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }
}
