package com.sjtu.project.common.exception;

public class TimeRangeOccupiedException extends RuntimeException{
    public TimeRangeOccupiedException() {
        super();
    }

    public TimeRangeOccupiedException(String message) {
        super(message);
    }
}
