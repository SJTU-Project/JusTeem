package com.sjtu.project.common.exception;

public class ObjectAlreadyExistsException extends RuntimeException{
    public ObjectAlreadyExistsException() {
        super();
    }

    public ObjectAlreadyExistsException(String message) {
        super(message);
    }
}
