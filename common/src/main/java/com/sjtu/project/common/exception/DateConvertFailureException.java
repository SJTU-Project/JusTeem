package com.sjtu.project.common.exception;

public class DateConvertFailureException extends RuntimeException{
    public DateConvertFailureException() {
        super();
    }

    public DateConvertFailureException(String message) {
        super(message);
    }
}
