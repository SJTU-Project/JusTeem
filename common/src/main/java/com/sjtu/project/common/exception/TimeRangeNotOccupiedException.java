package com.sjtu.project.common.exception;

public class TimeRangeNotOccupiedException extends RuntimeException{
    public TimeRangeNotOccupiedException() {
        super();
    }

    public TimeRangeNotOccupiedException(String message) {
        super(message);
    }
}
