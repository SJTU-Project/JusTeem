package com.sjtu.project.common.entity;

import lombok.Data;

@Data
public class ImageURL {
    String imageName = "";

    String url = "";
}
