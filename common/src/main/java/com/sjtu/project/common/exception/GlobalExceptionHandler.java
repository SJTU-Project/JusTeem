package com.sjtu.project.common.exception;

import com.mongodb.MongoWriteException;
import com.sjtu.project.common.response.Result;
import com.sjtu.project.common.response.ResultCode;
import com.sjtu.project.common.util.ResultUtil;
import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.sjtu.project.common.response.ResultCode.*;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler()
    @ResponseBody
    ResponseEntity handle(ObjectNotFoundException e) {
        return generateFailResponseEntity(OBJECT_NOT_FOUND, HttpStatus.NOT_FOUND, e.getMessage());
    }

    @ExceptionHandler()
    @ResponseBody
    ResponseEntity handle(ObjectAlreadyExistsException e) {
        return generateFailResponseEntity(OBJECT_ALREADY_EXISTS, HttpStatus.OK, e.getMessage());
    }

    @ExceptionHandler
    @ResponseBody
    ResponseEntity handle(IllegalArgumentException e) {
        String[] messages = e.getMessage().split(ResultUtil.SPLITTER);
        return generateFailResponseEntity(ILLEGAL_ARGUMENT, HttpStatus.OK, messages);
    }

    @ExceptionHandler
    @ResponseBody
    ResponseEntity handle(ResourceNotMatchException e) {
        return generateFailResponseEntity(RESOURCE_NOT_MATCH, HttpStatus.OK, e.getMessage());
    }

    @ExceptionHandler
    @ResponseBody
    ResponseEntity handle(TimeRangeOccupiedException e) {
        return generateFailResponseEntity(TIME_RANGE_OCCUPIED, HttpStatus.OK, e.getMessage());
    }

    @ExceptionHandler
    @ResponseBody
    ResponseEntity handle(TimeRangeNotOccupiedException e) {
        return generateFailResponseEntity(TIME_RANGE_NOT_OCCUPIED, HttpStatus.OK, e.getMessage());
    }

    @ExceptionHandler
    @ResponseBody
    ResponseEntity handle(MongoWriteException e) {
        Result result = ResultUtil.failure(e.getMessage(), DUPLICATE_KEY.getCode());
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @ExceptionHandler
    @ResponseBody
    ResponseEntity handle(FeignException e) {
        Result result = ResultUtil.failure(e.getMessage(), FEIGN_EXCEPTION.getCode());
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @ExceptionHandler
    @ResponseBody
    ResponseEntity handle(ServiceInvokedFailureException e) {
        String[] messages = e.getMessage().split(ResultUtil.SPLITTER);
        return generateFailResponseEntity(SERVICE_INVOKED_FAILURE, HttpStatus.OK, messages);
    }

    @ExceptionHandler
    @ResponseBody
    ResponseEntity handle(DateConvertFailureException e) {
        return generateFailResponseEntity(DATE_CONVERT_FAILURE, HttpStatus.OK, e.getMessage());
    }

    @ExceptionHandler
    @ResponseBody
    ResponseEntity handle(NoSuitableObjectException e) {
        return generateFailResponseEntity(NO_SUITABLE_OBJECT, HttpStatus.OK);
    }

    private ResponseEntity generateFailResponseEntity(ResultCode resultCode, HttpStatus ok, String... message) {
        String unformattedMessage = resultCode.getMessage();
        int code = resultCode.getCode();
        Result result = ResultUtil.failure(String.format(unformattedMessage, message), code);
        return new ResponseEntity(result, ok);
    }

}
