package com.sjtu.project.common.exception;

public class ResourceNotMatchException extends RuntimeException{
    public ResourceNotMatchException() {
        super();
    }

    public ResourceNotMatchException(String message) {
        super(message);
    }
}
