package com.sjtu.project.common.util;

import com.sjtu.project.common.entity.TimeRange;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.sjtu.project.common.util.ResultUtil.SPLITTER;

public class TimeRangeUtil {
    // 预约时间为从早上8点到晚上10点，以半小时为时间间隔
    public static int ONE_DAY_TIME_RANGE_SIZE = 28;

    static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

    public static String convertDateNumber2String(int dateNumber) {
        String HH = String.valueOf(dateNumber / 2);
        if (HH.length() == 1) {
            HH = "0" + HH;
        }
        String mm = dateNumber % 2 == 0 ? "00" : "30";
        return HH + ":" + mm;
    }

    public static int convertDateNumber2Index(int dateNumber) {
        if (dateNumber < 16 || dateNumber > 44) {
            throw new IllegalArgumentException("DateNumber" + SPLITTER + "DateNumber should between 16 and 44");
        }
        return dateNumber - 16;
    }

    public static int convertIndex2DateNumber(int index) {
        if (index < 0 || index > 28) {
            throw new IllegalArgumentException("TimeRangeIndex" + SPLITTER + "Index should between 0 and 28");
        }
        return index + 16;
    }

    public static void validateTimeRange(TimeRange timeRange) {
        if (timeRange.getStart() < 16 || timeRange.getStart() > 44
                || timeRange.getEnd() < 16 || timeRange.getEnd() > 44) {
            throw new IllegalArgumentException("DateNumber" + SPLITTER + "DateNumber should between 16 and 44");
        }
        if (timeRange.getStart() >= timeRange.getEnd()) {
            throw new IllegalArgumentException("TimeRange" + SPLITTER + "Start time should before end time");
        }
    }

    // 向后取整
    public static int convertTime2DateNumber(Date date) {
        String time = sdf.format(date);
        String[] splitter = time.split(":");
        int dateNumber = Integer.valueOf(splitter[0]) * 2 + (Integer.valueOf(splitter[1]) + 30) / 30;
        return dateNumber;
    }

    public static int convertCurrentTime2DateNumber() {
        return convertTime2DateNumber(new Date());
    }
}
