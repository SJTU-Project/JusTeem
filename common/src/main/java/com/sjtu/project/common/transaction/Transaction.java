package com.sjtu.project.common.transaction;

public interface Transaction {
    void execute();

    void validate();
}
