package com.sjtu.project.common.util;

import com.sjtu.project.common.exception.DateConvertFailureException;
import org.apache.commons.lang.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static com.sjtu.project.common.util.ResultUtil.SPLITTER;

public class DateUtil {
    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


    static Pattern pattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");

    public static List<Date> getOneWeekDates(Date date) {
        List<Date> res = new ArrayList<>();
        for (int i=0; i<7; ++i) {
            res.add(DateUtils.addDays(date, i));
        }
        return res;
    }

    public static String convert2String(Date date) {
        return sdf.format(date);
    }

    public static Date convertTime2Date(String time) {
        try {
            return DateUtils.parseDateStrictly(time, new String[] {"yyyy-MM-dd HH:mm"});
        } catch (ParseException e) {
            throw new DateConvertFailureException(time);
        }
    }

    public static Date convertDay2Date(String day) {
        try {
            return DateUtils.parseDateStrictly(day, new String[] {"yyyy-MM-dd"});
        } catch (ParseException e) {
            throw new DateConvertFailureException(day);
        }
    }

    public static void validateDate(String date) {
        if (!pattern.matcher(date).matches()) {
            throw new IllegalArgumentException("Date" + SPLITTER + "Date should be validate");
        }
    }
}
