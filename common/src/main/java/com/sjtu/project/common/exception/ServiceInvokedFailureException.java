package com.sjtu.project.common.exception;

public class ServiceInvokedFailureException extends RuntimeException{
    public ServiceInvokedFailureException() {
        super();
    }

    public ServiceInvokedFailureException(String message) {
        super(message);
    }
}
